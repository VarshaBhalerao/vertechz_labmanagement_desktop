package com.dineshonjava.dao;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;

import java.util.Date;
import java.util.List;
import java.text.*;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dineshonjava.hbUtil.HibernateUtil;
import com.dineshonjava.model.Active;
import com.dineshonjava.model.Composition;

import com.dineshonjava.model.formulation;
import com.dineshonjava.model.image;
import com.dineshonjava.model.recordcomment;
import com.dineshonjava.model.scancopy;
import com.dineshonjava.model.selflifestudy;
import com.dineshonjava.model.Records;
import com.dineshonjava.model.chief_login;
import com.dineshonjava.model.coldtest;
import com.dineshonjava.model.coldtestdays;
import com.dineshonjava.model.analysis;
import com.dineshonjava.model.analyticalstudy;
import com.dineshonjava.model.analyticalstudy1;
import com.dineshonjava.model.ats;
import com.dineshonjava.model.atsstudies;
import com.dineshonjava.model.atsstudies1;

//
@Repository("employeeDao")
public class EmployeeDaoImpl implements EmployeeDao {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<chief_login> listChief() {
		
		
		
		System.out.println("List of Emoployee");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		List<chief_login> chiefList =  session.createCriteria(chief_login.class).list();
		if(chiefList.isEmpty())
		{
			System.out.println("List is Empty... In dao");
		}
		trans.commit();
		return chiefList;
		
		
	}
	

    /* varsha  start*/
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<formulation> listFormulation() {
		System.out.println("List of formulation");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		List<formulation> fList =  session.createCriteria(formulation.class).addOrder(Order.desc("id")).list();
		trans.commit();
		return fList;
	}
	@Override
	public formulation getFormulation(int id) {
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		formulation fbean =  (formulation) session.get(formulation.class, id);
		trans.commit();
		return fbean;
 	}	
	
	public void addFormulation(formulation formulation) {		
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		   session.saveOrUpdate(formulation);
		   trans.commit();		 
	}
	@Override
	public void deleteFormulation(formulation formulationBean) {
		System.out.println("in  dao delete formualtion..");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		 session.createQuery("DELETE FROM formulation WHERE id = "+formulationBean.getId()).executeUpdate();	
		 trans.commit();
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<formulation> getFormulationList(int result, int offset_real) {
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		Criteria criteria = session.createCriteria(formulation.class).addOrder(Order.desc("id"));
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<formulation>  formulationList = (List<formulation>)criteria.list();
		trans.commit();
		return formulationList;
	}
	@Override
	@Transactional
	public int getFormulationSize() {
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		int size =  session.createCriteria(formulation.class).list().size();
		trans.commit();
		return size;
	}
	
	
	
	

	/*public void addAnalysis(analysis analysis) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().saveOrUpdate(analysis);		
	}*/
	/*@Override
>>>>>>> branch 'master' of https://VarshaBhalerao@bitbucket.org/VarshaBhalerao/vertechz_labmanagement_desktop.git
	public analysis getAnalysis(int id) {
<<<<<<< HEAD
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		analysis abean = (analysis) session.get(analysis.class, id);
		trans.commit();
		return abean;
		
	}
=======
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (analysis) sessionFactory.getCurrentSession().get(analysis.class, id);
	}*/

	@SuppressWarnings("unchecked")
	@Override
	public List<analysis> listAnalysis() {

		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		List<analysis> aList = session.createCriteria(analysis.class).list();
		trans.commit();
		return aList;

		/*SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<analysis>) sessionFactory.getCurrentSession().createCriteria(analysis.class).list();*/
		
		

	} 
	
	
	@Override
	public analysis getAnalysis(int id) {
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		   analysis fbean =  (analysis) session.get(analysis.class, id);
		trans.commit();
		return fbean;
 	}	
	
	
	@Override
	public void deleteAnalysis(analysis analysisBean) {
		System.out.println("in  dao delete Analysis..");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		 session.createQuery("DELETE FROM analysis WHERE id = "+analysisBean.getId()).executeUpdate();	
		 trans.commit();
	}
	
	
	/*@Override
	public void deleteAnalysis(analysis analysisBean) {
<<<<<<< HEAD
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		   session.createQuery("DELETE FROM analysis WHERE id = "+analysisBean.getId()).executeUpdate();
		   trans.commit();
	}	
=======
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().createQuery("DELETE FROM analysis WHERE id = "+analysisBean.getId()).executeUpdate();		
	}*/	

	
	
	
	
	public void addATS(ats atsBean) {	
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		   session.saveOrUpdate(atsBean);
		   trans.commit();	
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<ats> listATS() {
		System.out.println("List of ats");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		List<ats> fList =  session.createCriteria(ats.class).addOrder(Order.desc("id")).list();
		trans.commit();
		return fList;
	}
	@Override
	public ats getATS(int id) {
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		  ats fbean =  (ats) session.get(ats.class, id);
		trans.commit();
		return fbean;
	}
	@Override
	public void deleteATS(ats atsBean) {
		System.out.println("in  dao delete ats..");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		 session.createQuery("DELETE FROM ats WHERE id = "+atsBean.getId()).executeUpdate();	
		 trans.commit();	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<ats> getATSList(int result, int offset_real) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ats.class).addOrder(Order.desc("id"));
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<ats>  atsList = (List<ats>)criteria.list();
		return atsList;
	}
	@Override
	@Transactional
	public int getATS_Size() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return sessionFactory.getCurrentSession().createCriteria(ats.class).list().size();
	}
	
	
	
	
	
	public void addColdTestDays(coldtestdays coldtestdaysBean) {
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		   session.saveOrUpdate(coldtestdaysBean);
		   trans.commit();	
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<coldtestdays> listColdTestDays() {
		System.out.println("List of coldtestdays");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		List<coldtestdays> fList =  session.createCriteria(coldtestdays.class).addOrder(Order.desc("id")).list();
		trans.commit();
		return fList;
	}
	@Override
	public coldtestdays getColdTestDays(int id) {
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		   coldtestdays fbean =  (coldtestdays) session.get(coldtestdays.class, id);
		trans.commit();
		return fbean;
	}
	@Override
	public void deleteColdTestDays(coldtestdays coldtestdaysBean) {
		System.out.println("in  dao delete coldtestdays..");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		 session.createQuery("DELETE FROM coldtestdays WHERE id = "+coldtestdaysBean.getId()).executeUpdate();	
		 trans.commit();
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<coldtestdays> getColdTestDaysList(int result, int offset_real) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(coldtestdays.class).addOrder(Order.desc("id"));
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<coldtestdays> coldtestdaysList = (List<coldtestdays>)criteria.list();
		return coldtestdaysList;
	}
	@Override
	@Transactional
	public int getColdTestDays_Size() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return sessionFactory.getCurrentSession().createCriteria(coldtestdays.class).list().size();
	}

	
		
	public void addImage(image imageBean) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().saveOrUpdate(imageBean);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<image> listImage() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<image>) sessionFactory.getCurrentSession().createCriteria(image.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<image> listImage(int rid) {
		String hql = "FROM image i WHERE i.rid = :r_id";
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<image> results = query.list();
		return results;	
	}
	@Override
	public image getImage(int id) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (image) sessionFactory.getCurrentSession().get(image.class, id);
	}
	@Override
	public void deleteImage(image imageBean) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().createQuery("DELETE FROM image WHERE id = "+imageBean.getId()).executeUpdate();
	}

	
	/* varsha end*/
	
	
	
	
	
	
	
	
	
	/*Bhushan Start*/
	
	
	//Record Comment Start

		public void addRecord_Comment(recordcomment recordcomment) {
			SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
			sessionFactory.getCurrentSession().saveOrUpdate(recordcomment);
			
		}

		@SuppressWarnings("unchecked")
		@Override
		public List<recordcomment> getRecord_Comment(int rid) {
			String hql = "FROM recordcomment a WHERE a.rid = :r_id";
			SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			query.setParameter("r_id",rid);			
			List<recordcomment> results = query.list();
			return results;
		}


		@Override
		public void DeleteRecord_Comment(recordcomment recordcomment) {
			SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
			sessionFactory.getCurrentSession().createQuery("DELETE FROM recordcomment WHERE comment_id = "+recordcomment.getComment_id()).executeUpdate();

			
		}

		//Record Comment End
	
		public void addRecords(Records records) {
			Session session= HibernateUtil.getNowSession();
			   Transaction trans=session.beginTransaction();
			   session.saveOrUpdate(records);
			   trans.commit();		
			
		}
	@Override
	public List<Records> listRecords() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<Records>) sessionFactory.getCurrentSession().createCriteria(Records.class).list();
	}
	public Records getRecords(int rid) {
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		   Records fbean =  (Records) session.get(Records.class, rid);
		trans.commit();
		return fbean;
	}
	@Override
	public void deleteRecord(Records records) {
		System.out.println("record id in dao = " + records.getRid());
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().createQuery("DELETE FROM records WHERE rid = "+records.getRid()).executeUpdate();
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Records> getRecordsList(int result, int offset_real) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Records.class).addOrder(Order.desc("rid"));
		criteria.setFirstResult(offset_real);
		criteria.setMaxResults(result);
		List<Records>  recordsList = (List<Records>)criteria.list();
		return recordsList;
	}
	@Transactional
	public int getRecords_Size() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return sessionFactory.getCurrentSession().createCriteria(Records.class).list().size();
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Active> listActive() {
		System.out.println("List of Active");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		List<Active> fList =  session.createCriteria(Active.class).addOrder(Order.desc("active_id")).list();
		trans.commit();
		return fList;
	}	
	@Override
	public List<Active> listActive(int rid) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		String hql = "FROM active a WHERE a.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<Active> results = query.list();
		return results;		
	}	
	public void addActive(Active active) {
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		   session.saveOrUpdate(active);
		   trans.commit();		
	}
	@Override
	public void deleteActive(Active active) {
		System.out.println("in  dao delete active..");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		 session.createQuery("DELETE FROM active WHERE id = "+active.getActive_id()).executeUpdate();	
		 trans.commit();
	}
	@Override
	public Active getActive(int active_id) {
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		   Active fbean =  (Active) session.get(Active.class, active_id);
		trans.commit();
		return fbean;
	}

	
	
	@SuppressWarnings("unchecked") 
	@Override
	public List<Composition> listComposition() {
		//SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		//return (List<Composition>) sessionFactory.getCurrentSession().createCriteria(Composition.class).list();
		System.out.println("List of Composition");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		List<Composition> fList =  session.createCriteria(Composition.class).addOrder(Order.desc("composition_id")).list();
		trans.commit();
		return fList;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Composition> listComposition(int rid) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		String hql = "FROM composition c WHERE c.rid = :r_id ORDER BY c.constituent";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<Composition> results = query.list();
		return results;
	}
	public void addComposition(Composition composition) {
		//SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		//sessionFactory.getCurrentSession().saveOrUpdate(composition);
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		   session.saveOrUpdate(composition);
		   trans.commit();
	}
	@Override
	public void deleteComposition(Composition composition) {
		//SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		//sessionFactory.getCurrentSession().createQuery("DELETE FROM composition WHERE composition_id = "+composition.getComposition_id()).executeUpdate();
		System.out.println("in  dao delete Composition..");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		 session.createQuery("DELETE FROM composition WHERE composition_id = "+composition.getComposition_id()).executeUpdate();	
		 trans.commit();
	}
	@Override
	public Composition getComposition(int conposition_id) {
		//SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		//return (Composition) sessionFactory.getCurrentSession().get(Composition.class, conposition_id);
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		   Composition fbean =  (Composition) session.get(Composition.class, conposition_id);
		trans.commit();
		return fbean;
	}
	
	
	
	@Override
	public selflifestudy getSelfLife_Studies(int selflife_id) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (selflifestudy) sessionFactory.getCurrentSession().get(selflifestudy.class, selflife_id);

	}


	@Override
	public void deleteSelfLife_Studies(selflifestudy selflifestudy) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().createQuery("DELETE FROM selflifestudy WHERE Selflife_id = "+selflifestudy.getSelflife_id()).executeUpdate();

	}


	
	public void addSelfLife_Studies(selflifestudy selflifestudy) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().saveOrUpdate(selflifestudy);
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<selflifestudy> listSelfLife_Studies() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<selflifestudy>) sessionFactory.getCurrentSession().createCriteria(selflifestudy.class).list();

	}


	@SuppressWarnings("unchecked")
	@Override
	public List<selflifestudy> listSelfLife_Studies(int rid) {
		String hql = "FROM selflifestudy a WHERE a.rid = :r_id";
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<selflifestudy> results = query.list();
		return results;
	}
	
	
	
	public void addAnalytical_Studies(analyticalstudy analyticalstudy) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().saveOrUpdate(analyticalstudy);		
	}
	
	@Override
	public void addAnalytical_Studies1(int analyticalstudy_id, String active, String concentration, int rid) {
		//sessionFactory.getCurrentSession().createQuery("insert into analyticalstudy1 (analyticalstudy_id, analytical_active, concentration) values ("+analyticalstudy_id+", '"+active+"', '"+concentration+"')").executeUpdate();
		
		Session session= HibernateUtil.getNowSession();
	
	    Transaction t=session.beginTransaction();
	    
	    analyticalstudy1 analyticalstudy1 = new analyticalstudy1();
	    analyticalstudy1.setAnalyticalstudy_id(analyticalstudy_id);
	    analyticalstudy1.setAnalytical_active(active);
	    analyticalstudy1.setConcentration(concentration);
	    analyticalstudy1.setRid(rid);
	    session.persist(analyticalstudy1);
	    t.commit();
	    session.close();
	}
	
	@Override
	public void updateAnalytical_Studies1(int analyticalstudy_id, String active, String concentration, int rid) {
		
		
		Session session= HibernateUtil.getNowSession();
		 
		//sessionFactory.getCurrentSession().createQuery("update analyticalstudy1 set analytical_active= '"+active+"', concentration='"+concentration+"' where analyticalstudy_id1="+ analyticalstudy_id).executeUpdate();
		
	    Transaction t=session.beginTransaction();
	    
	  Query query1=session.createQuery("update analyticalstudy1 set analytical_active=:a, concentration=:c where analyticalstudy1_id=:n");
	    query1.setParameter("a" , active);
	    query1.setParameter("c" , concentration);
	    query1.setParameter("n" , analyticalstudy_id);
	    //query1.setParameter("r" , rid);
	    int count=query1.executeUpdate();
	    t.commit();
	    session.close();
		
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<analyticalstudy> listAnalytical_Studies() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<analyticalstudy>) sessionFactory.getCurrentSession().createCriteria(analyticalstudy.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<analyticalstudy> listAnalytical_Studies(int rid) {
		String hql = "FROM analyticalstudy a WHERE a.rid = :r_id";
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<analyticalstudy> results = query.list();
		return results;
	}
	
	@Override
	public void deleteAnalytical_Studies1(String active, int rid) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().createQuery("DELETE FROM analyticalstudy1 WHERE analytical_active = '"+active+"' AND rid="+rid).executeUpdate();

		
	}
	
	@Override
	public void deleteAnalytical_Studies1(int analyticalstudy_id) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().createQuery("DELETE FROM analyticalstudy1 WHERE analyticalstudy_id = "+analyticalstudy_id).executeUpdate();

		
	}
	
	@Override
	public void deleteAnalytical_Studies(analyticalstudy analyticalstudy) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().createQuery("DELETE FROM analyticalstudy WHERE analyticalstudy_id = "+analyticalstudy.getAnalyticalstudy_id()).executeUpdate();
	}
	@Override
	public analyticalstudy getAnalytical_Studies(int analyticalstudy_id) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (analyticalstudy) sessionFactory.getCurrentSession().get(analyticalstudy.class, analyticalstudy_id);
	}


	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<scancopy> listScanned_Copies() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<scancopy>) sessionFactory.getCurrentSession().createCriteria(scancopy.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<scancopy> listScanned_Copies(int rid) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		String hql = "FROM scancopy s WHERE s.rid = :r_id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<scancopy> results = query.list();
		return results;
	}
	@Override
	public void addScanned_Copies(scancopy scancopy) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().saveOrUpdate(scancopy);		
	}
	@Override
	public void deleteScanned_Copies(scancopy scancopy) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().createQuery("DELETE FROM scancopy WHERE scan_id = "+scancopy.getScan_id()).executeUpdate();		
	}
	@Override
	public scancopy getScanned_Copies(int scan_id) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (scancopy) sessionFactory.getCurrentSession().get(scancopy.class, scan_id);
	}


	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<coldtest> listCold_Test() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<coldtest>) sessionFactory.getCurrentSession().createCriteria(coldtest.class).list();
	}	
	@SuppressWarnings("unchecked")
	@Override
	public List<coldtest> listCold_Test(int rid) {
		String hql = "FROM coldtest c WHERE c.rid = :r_id";
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<coldtest> results = query.list();
		return results;
	}	
	public void addCold_Test(coldtest coldtest) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().saveOrUpdate(coldtest);		
	}	
	public void deleteCold_Test(coldtest coldtest) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().createQuery("DELETE FROM coldtest WHERE cold_id = "+coldtest.getCold_id()).executeUpdate();		
	}
	@Override
	public coldtest getCold_Test(int cold_id) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (coldtest) sessionFactory.getCurrentSession().get(coldtest.class, cold_id);
	}


	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<atsstudies> listATS_Studies() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<atsstudies>) sessionFactory.getCurrentSession().createCriteria(atsstudies.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<atsstudies> listATS_Studies(int rid) {
		String hql = "FROM atsstudies a WHERE a.rid = :r_id";
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("r_id",rid);
		List<atsstudies> results = query.list();
		return results;
	}
	@Override
	public atsstudies getATS_Studies(int ats_id) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (atsstudies) sessionFactory.getCurrentSession().get(atsstudies.class, ats_id);
	}
	@Override
	public void deleteATS_Studies(atsstudies atsstudies) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().createQuery("DELETE FROM atsstudies WHERE ats_id = "+atsstudies.getAts_id()).executeUpdate();		
	}
	public void addATS_Studies(atsstudies atsstudies) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().saveOrUpdate(atsstudies);		
	}

	
//SEARCH RECORDS OF CHIEF ONLY
	
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchBy_Status_Active(String status, String active, String username) {
									SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where status='"+status+"' And records_active LIKE'%"+active+"%' AND research_executive='"+username+"'").list();
							
								}
							
							
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchBy_Fomulation_Active(String formulation, String active, String username) {
									SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' And records_active LIKE'%"+active+"%' AND research_executive='"+username+"'").list();
							
								}
							
							
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchBy_Fomulation_Status(String formulation, String status, String username) {
									SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' AND status='"+status+"'AND research_executive='"+username+"'").list();
							
								}
							
							
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchBy_Fomulation_Status_Active(String formulation, String status, String active, String username) {
									SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' AND status='"+status+"' And records_active LIKE'%"+active+"%'AND research_executive='"+username+"'").list();
							
								}
								
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchByFormulation(String formulation, String username) {
									// TODO Auto-generated method stub
									SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"'AND research_executive='"+username+"'").list();
								}
							
							
							
							
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchByActive(String active, String username) {
									SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where research_executive='"+username+"'And ( records_active='"+active+"'OR records_active LIKE '%"+active+"%')").list();
							
								}
							
							
							
							
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchByStatus(String status, String username) {
									SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where status='"+status+"'AND research_executive='"+username+"'").list();
							
								}
							
							
							
							
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchByDate(Date todate, Date fromdate, String username) {
									/*SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
									Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Records.class)
											   .add(Restrictions.between("date", s.format(fromdate) , s.format(todate) ));
											List<Records> listCustomer = criteria.list();
											return listCustomer;*/
									SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
									SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date BETWEEN '"+s.format(fromdate)+"' AND '"+s.format(todate)+"'AND research_executive='"+username+"'").list();
									
									
								}
								
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> ChiefRecordSerchBySingleDate(Date todate, Date fromdate, String username) {
									SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
									SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date='"+s.format(todate)+"' or date='"+s.format(fromdate)+"'AND research_executive='"+username+"'").list();
									
								}
								
								@SuppressWarnings("unchecked")
								@Override
								public List<Records> listChiefRecord(String username) {
									SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
									return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where research_executive='"+username+"'").list();
									
								}

	
	//SEARCH RECORDS OF CHIEF ONLY
	
	
// Search for all records
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchBy_Status_Active(String status, String active) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where status='"+status+"' And records_active LIKE'%"+active+"%'").list();

	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchBy_Fomulation_Active(String formulation, String active) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' And records_active LIKE'%"+active+"%'").list();

	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchBy_Fomulation_Status(String formulation, String status) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' AND status='"+status+"'").list();

	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchBy_Fomulation_Status_Active(String formulation, String status, String active) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"' AND status='"+status+"' And records_active LIKE'%"+active+"%'").list();

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchByFormulation(String formulation) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where formulation='"+formulation+"'").list();
	}




	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchByActive(String active) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where records_active='"+active+"'OR records_active LIKE '%"+active+"%'").list();

	}




	@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchByStatus(String status) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where status='"+status+"'").list();

	}




	/*@SuppressWarnings("unchecked")
	@Override
	public List<Records> SerchByDate(Date todate, Date fromdate, String username) {
		SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Records.class)
				   .add(Restrictions.between("date", s.format(fromdate) , s.format(todate) ));
				List<Records> listCustomer = criteria.list();
				return listCustomer;
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date BETWEEN '"+s.format(fromdate)+"' AND '"+s.format(todate)+"'AND research_executive='"+username+"'").list();
		
		
	}
	*/
	@SuppressWarnings("unchecked")
	public List<Records> SerchByDate(LocalDate todate, LocalDate fromdate) {
		SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Records.class)
				   .add(Restrictions.between("date1", s.format(fromdate) , s.format(todate) ));
				List<Records> listCustomer = criteria.list();
				return listCustomer;
	}
	
	@SuppressWarnings("unchecked")
	public List<Records> SerchBySingleDate(LocalDate todate, LocalDate fromdate) {
		SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date='"+s.format(todate)+"' or date='"+s.format(fromdate)+"'").list();
		
	}




	@SuppressWarnings("unchecked")
	@Override
	public List<Records> TodayRecordCount(Date format) throws HibernateException, ParseException {
		SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		System.out.println("Todays date is"+s.format(format));
		String date=s.format(format);
		DateFormat df = new SimpleDateFormat("mm/dd/yyyy");
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		
			
			
				return (List<Records>) sessionFactory.getCurrentSession().createQuery("from records where date='"+s.format(format)+"'").list();
			
		

		

	}




	@SuppressWarnings("unchecked")
	@Override
	public List<Active> List_Chief_Preview_Active(int rid) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<Active>) sessionFactory.getCurrentSession().createQuery("from active where rid="+rid).list();
		
	}




	@SuppressWarnings("unchecked")
	@Override
	public List<Composition> List_Chief_Preview_Composition(int rid) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<Composition>) sessionFactory.getCurrentSession().createQuery("from composition where rid="+rid).list();
		
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<analyticalstudy> List_Chief_Preview_Analytical_Studies(int rid) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<analyticalstudy>) sessionFactory.getCurrentSession().createQuery("from analyticalstudy where rid="+rid).list();
		
	}




	@SuppressWarnings("unchecked")
	@Override
	public List<atsstudies> List_Chief_Preview_ATS_Studies(int rid) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<atsstudies>) sessionFactory.getCurrentSession().createQuery("from atsstudies where rid="+rid).list();
		
	}




	@SuppressWarnings("unchecked")
	@Override
	public List<coldtest> List_Chief_Preview_Cold_Test(int rid) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<coldtest>) sessionFactory.getCurrentSession().createQuery("from coldtest where rid="+rid).list();
		
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<chief_login> ListChiefLogin() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (List<chief_login>) sessionFactory.getCurrentSession().createCriteria(chief_login.class).list();

	}

	
	
	
	
	@Override
	public void addATS_Studies1(int ats_id, String active, String concentration, int rid) {
		Session session= HibernateUtil.getNowSession();
		
	   
	    Transaction t=session.beginTransaction();
	    
	    atsstudies1 atsstudies1 = new atsstudies1();
	    atsstudies1.setAts_id(ats_id);;
	    atsstudies1.setAts_active(active);;
	    atsstudies1.setConcentration(concentration);;
	    atsstudies1.setRid(rid);;
	    session.persist(atsstudies1);
	    t.commit();
	    session.close();
		
	}


	@Override
	public void deleteATS_Studies1(String activevalue, int rid) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().createQuery("DELETE FROM atsstudies1 WHERE ats_active = '"+activevalue+"' AND rid="+rid).executeUpdate();

		
	}


	@Override
	public void deleteATS_Studies1(int ats_id) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		sessionFactory.getCurrentSession().createQuery("DELETE FROM atsstudies1 WHERE ats_id = "+ats_id).executeUpdate();

		
	}


	@Override
	public void updateATS_Studies1(int ats_id, String active, String concentration, int rid) {
		
		//sessionFactory.getCurrentSession().createQuery("update analyticalstudy1 set analytical_active= '"+active+"', concentration='"+concentration+"' where analyticalstudy_id1="+ analyticalstudy_id).executeUpdate();
		
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		System.out.println("in  dao Employee get..");
		String hql = "FROM chief_login s WHERE s.designation = :designation";
		
		Query query = session.createQuery("update atsstudies1 set ats_active=:a, concentration=:c where ats1_id=:n");
		query.setParameter("a" , active);
	    query.setParameter("c" , concentration);
	    query.setParameter("n" , ats_id);
	    //query1.setParameter("r" , rid);
	    int count=query.executeUpdate();
	   
		
		trans.commit();
		
		
		
		
	    
	 
	    
	    
	}
	/*Bhushan end*/
	
	
	
	
	/*Dipali start*/

	@Override
	public void addchief(chief_login cf) {
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		   session.saveOrUpdate(cf);
		   trans.commit();
	}
	@Override
	public void addManager(chief_login manager) {
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		   session.saveOrUpdate(manager);
		   trans.commit();
	
	}





	

	
	@Override
	public List<chief_login> getEmployeeList(String chief) {
		// TODO Auto-generated method stub
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		System.out.println("in  dao Employee get..");
		String hql = "FROM chief_login s WHERE s.designation = :designation";
		
		Query query = session.createQuery(hql);
				
		query.setParameter("designation",chief);
		List<chief_login> results = query.list();
		trans.commit();
		return results;
		
	}
	
	@Override
	public void addManager3(chief_login manager) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public chief_login getEmp(int emp_id) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		return (chief_login) sessionFactory.getCurrentSession().get(chief_login.class,emp_id);
	}




	@Override
	public void deleteEmp(chief_login managerBean) {
		// TODO Auto-generated method stub
		System.out.println("in  dao delete emp..");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		

		session.createQuery("DELETE FROM chief_login WHERE id = "+managerBean.getId()).executeUpdate();		
		trans.commit();
	}
 
	
		@Override
	public List<Records> listRemainder1(String userid) {
		// TODO Auto-generated method stub
		System.out.println("In List from username Remaindder");
		//return (List<Records>) sessionFactory.getCurrentSession().createCriteria(Records.class).list();
		
		Date date=new Date();
		Calendar calendar = Calendar.getInstance();
		date=calendar.getTime(); 
		SimpleDateFormat s;
		//   s=new SimpleDateFormat("MM/dd/yyyy");

		s=new SimpleDateFormat("yyyy/MM/dd");
		String currentdate=s.format(date);

		System.out.println("Date is"+currentdate);
		System.out.println(s.format(date));
		
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		
		return(List<Records>)sessionFactory.getCurrentSession().createQuery("from records where reminder_date >='"+currentdate+"' and research_executive='"+userid+"' ").list();
	}
	
	

	/*Dipali end*/
	
	
	@Override
	public List<Records> listRemainder() {
		
		System.out.println("In List Remaindder");
		//return (List<Records>) sessionFactory.getCurrentSession().createCriteria(Records.class).list();
		
		Date date=new Date();
		Calendar calendar = Calendar.getInstance();
		date=calendar.getTime(); 
		SimpleDateFormat s;
		//   s=new SimpleDateFormat("MM/dd/yyyy");

		s=new SimpleDateFormat("yyyy/MM/dd");
		String currentdate=s.format(date);

		System.out.println("Date is"+currentdate);
		System.out.println(s.format(date));
		
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		
		return(List<Records>)sessionFactory.getCurrentSession().createQuery("from records where reminder_date >='"+currentdate+"'").list();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Records> ListRecords() {
		//SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		//return (List<Records>) sessionFactory.getCurrentSession().createCriteria(Records.class).list();
		
		System.out.println("List of Records");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		List<Records> fList =  session.createCriteria(Records.class).addOrder(Order.desc("id")).list();
		trans.commit();
		return fList;

	}


	@Override
	public void addAnalysis(analysis analysis) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<analysis> getAnalysisList(int result, int offset_real) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int getAnalysis_Size() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public List<Records> SerchByDate1(LocalDate todate, LocalDate parsedDate2) {
		// TODO Auto-generated method stub
		
		System.out.println("Todate is in Dao"+todate);
		
		System.out.println("Prsed Date is in Dao"+parsedDate2);
SimpleDateFormat s= new SimpleDateFormat("yyyy/MM/dd");
		Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		//String hql = "from  records  where    date1= '"+todate+"'AND reminder_date1='"+parsedDate2+"'";
		String hql = "from records where date1 BETWEEN '"+todate+"' AND '"+parsedDate2+"'";
		Query query = session.createQuery(hql);
		
		List<Records> results = query.list();
		trans.commit();
		return results;
		
		
		/*Session session= HibernateUtil.getNowSession();
		   Transaction trans=session.beginTransaction();
		//SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Criteria criteria = session.createCriteria(Records.class)
				   .add(Restrictions.between("date1", s.format(todate), s.format(parsedDate2) ));
				List<Records> listCustomer = criteria.list();
				return listCustomer;*/


 /*Session session = HibernateUtil.getNowSession();

 Query    query =session.createQuery("FROM records AS c WHERE c.date1 BETWEEN :stDate AND :edDate ");
		 query.setParameter("stDate", todate);
		 query.setParameter("edDate", parsedDate2);
		 return query.list();*/
	
	}


	
	}


