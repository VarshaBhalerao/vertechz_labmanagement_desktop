package com.dineshonjava.hbUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import com.dineshonjava.model.*;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Deepak Mandal
 */
public class HibernateUtil {

	 private static final SessionFactory sessionFactory;
	    private static Session session;

	    static {
	    	
	        try {	           
		        	Configuration configuration = new Configuration();
		           // configuration.configure("hibernate.cfg.xml");
		        	configuration.addAnnotatedClass(Active.class);
		        	configuration.addAnnotatedClass(analysis.class);
		        	configuration.addAnnotatedClass(analyticalstudy.class);
		        	configuration.addAnnotatedClass(analyticalstudy1.class);
		        	configuration.addAnnotatedClass(ats.class);
		        	configuration.addAnnotatedClass(atsstudies.class);
		        	configuration.addAnnotatedClass(atsstudies1.class);
		        	configuration.addAnnotatedClass(chief_login.class);
		        	configuration.addAnnotatedClass(coldtest.class);
		        	configuration.addAnnotatedClass(coldtestdays.class);
		        	configuration.addAnnotatedClass(Composition.class);		        	
		        	configuration.addAnnotatedClass(formulation.class);
		        	configuration.addAnnotatedClass(image.class);
		        	configuration.addAnnotatedClass(recordcomment.class);
		        	configuration.addAnnotatedClass(Records.class);
		        	configuration.addAnnotatedClass(scancopy.class);
		        	configuration.addAnnotatedClass(selflifestudy.class);
		        	

		        	configuration.addAnnotatedClass(chief_login.class);
		        	configuration.addAnnotatedClass(analysis.class);
		        	configuration.addAnnotatedClass(ats.class);
		        	configuration.addAnnotatedClass(coldtestdays.class);
		        	configuration.addAnnotatedClass(Records.class);
		        	configuration.addAnnotatedClass(Active.class);
		        	configuration.addAnnotatedClass(Composition.class);


		            configuration.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
		            configuration.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/desktop_lab_management");
		            configuration.setProperty("hibernate.connection.username", "root");
		            configuration.setProperty("hibernate.connection.password", "root");
		            configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		            configuration.setProperty("hibernate.hbm2ddl.auto", "update");
		            configuration.setProperty("hibernate.current_session_context_class", "thread");
		            configuration.setProperty("hibernate.show_sql", "true");
		            configuration.setProperty("hibernate.format_sql", "true");
	
		            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
		            sessionFactory = configuration.buildSessionFactory(builder.build());// this is ging to be changed
	        } 
	        catch (Throwable ex) {
	            System.err.println("Initial SessionFactory creation failed." + ex);
	            throw new ExceptionInInitializerError(ex);
	        }
	    }

	    public static SessionFactory getSessionFactory() {
	        return sessionFactory;
	    }

	    public static Session getNowSession() {
	        if (session == null) {
	            session = getSessionFactory().openSession();
	        } else {
	            session = getSessionFactory().getCurrentSession();
	        }
	        return session;
	    }
}