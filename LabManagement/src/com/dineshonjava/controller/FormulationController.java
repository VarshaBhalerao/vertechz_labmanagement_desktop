package com.dineshonjava.controller;

import java.io.IOException;

import java.util.List;
import java.util.Optional;

import java.util.regex.Matcher;
import java.util.regex.Pattern;



import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Callback;
import com.dineshonjava.model.*;
import com.dineshonjava.service.EmployeeService;
import com.dineshonjava.service.EmployeeServiceImpl;

public class FormulationController {

	private EmployeeService employeeService = new EmployeeServiceImpl();
	@FXML
    private Button btnLogout;
	
	@FXML
    private Label formulationId;
	
	@FXML
    private TextField formulationType;
	
	@FXML
	private ComboBox<String> status;
	
	@FXML
	private TableView<formulation> userTable;

	@FXML
	private TableColumn<formulation, Integer> colFormulationId;

	@FXML
	private TableColumn<formulation, String> colFormulationType;

	@FXML
	private TableColumn<formulation, String> colStatus;

	@FXML
    private TableColumn<formulation, Boolean> colEdit;
	
	@FXML
    private MenuItem deleteUsers;
	
	private  Stage primaryStage;
	/*@Lazy
    @Autowired
    private StageManager stageManager;
	
	@Autowired
	private UserService userService;
	*/
	private ObservableList<formulation> userList = FXCollections.observableArrayList();
	private ObservableList<String> roles = FXCollections.observableArrayList("Enabled", "Disabled");
	
	@FXML
	private void showFormulation(ActionEvent event) {
		Platform.exit();
    }
	
	@FXML
	private void showAnalysis(ActionEvent event) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Formulation.fxml"));
		Scene scene = new Scene(root);
		primaryStage.close();
		Parent root1 = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Analysis.fxml"));
		Scene scene1 = new Scene(root1);
		primaryStage.setScene(scene1);
		
	
    }
	
	@FXML
	private void showATS(ActionEvent event) {
		Platform.exit();
    }
	
	@FXML
	private void showCold(ActionEvent event) {
		Platform.exit();
    }

	/**
	 * Logout and go to the login page
	 */
    @FXML
    private void logout(ActionEvent event) throws IOException {
    	//stageManager.switchScene(FxmlView.LOGIN);    	
    }
    
    @FXML
    void reset(ActionEvent event) {
    	clearFields();
    }
    
	
	
	@FXML
    private void saveUser(ActionEvent event){
    	
    	/*if(validate("Formulation Type", getFormulationType(), "[a-zA-Z]+") && emptyValidation("Status", getStatus() == null) ){*/
		if(validate("Formulation Type", getFormulationType(), "^[\\p{L} .'-]+$") && emptyValidation("Status", getStatus() == null) ){
    		if(formulationId.getText() == null || formulationId.getText() == ""){
    			
    				/*User user = new User();
        			user.setformulationType(getformulationType());
        			user.setLastName(getLastName());
        			user.setDob(getDob());
        			user.setGender(getGender());
        			user.setRole(getRole());
        			user.setEmail(getEmail());
        			user.setPassword(getPassword());
        			
        			User newUser = userService.save(user);*/
    			
    				formulation fbean = new formulation();
    			    fbean.setFormulationtype(getFormulationType());
    			    fbean.setStatus(getStatus());
    			    employeeService.addFormulation(fbean);
        			
        			saveAlert(fbean);
    			
    			
    		}else{
    			formulation fbean = employeeService.getFormulation(Integer.parseInt(formulationId.getText()));
    			fbean.setFormulationtype(getFormulationType());
    			fbean.setStatus(getStatus());
    			 employeeService.addFormulation(fbean);
    			
    			updateAlert(fbean);
    		}
    		
    		clearFields();
    		loadUserDetails();
    	}
    	
    	
    }
    
    @FXML
    private void deleteUsers(ActionEvent event){
    	List<formulation> formulationList= userTable.getSelectionModel().getSelectedItems();
    	
    	Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText(null);
		alert.setContentText("Are you sure you want to delete selected?");
		Optional<ButtonType> action = alert.showAndWait();
		
	if(action.get() == ButtonType.OK)
	{
		for(formulation formulationBean:formulationList)
		{
			employeeService.deleteFormulation(formulationBean);
		}
	}
    	
    	loadUserDetails();
    }
    
   	private void clearFields() {
		formulationId.setText(null);
		formulationType.clear();		
		status.getSelectionModel().clearSelection();
		
	}
	
	private void saveAlert(formulation fbean){
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("User saved successfully.");
		alert.setHeaderText(null);
		alert.setContentText("The user "+fbean.getFormulationtype()+" "+" has been created and  id is "+ fbean.getId() +".");
		alert.showAndWait();
	}
	
	private void updateAlert(formulation fbean){
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("User updated successfully.");
		alert.setHeaderText(null);
		alert.setContentText("The user "+fbean.getFormulationtype()+" has been updated.");
		alert.showAndWait();
	}
	
	

	public String getFormulationType() {
		return formulationType.getText();
	}

	
	
	public String getStatus() {
		return status.getSelectionModel().getSelectedItem();
	}

	
  

	public void initialize() {
		
		status.setItems(roles);
		
		userTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		setColumnProperties();
		
		// Add all users into table
		loadUserDetails();
	}
	
	
	
	/*
	 *  Set All userTable column properties
	 */
	private void setColumnProperties(){
		/* Override date format in table
		 * colDOB.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<LocalDate>() {
			 String pattern = "dd/MM/yyyy";
			 DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
		     @Override 
		     public String toString(LocalDate date) {
		         if (date != null) {
		             return dateFormatter.format(date);
		         } else {
		             return "";
		         }
		     }

		     @Override 
		     public LocalDate fromString(String string) {
		         if (string != null && !string.isEmpty()) {
		             return LocalDate.parse(string, dateFormatter);
		         } else {
		             return null;
		         }
		     }
		 }));*/
		
		colFormulationId.setCellValueFactory(new PropertyValueFactory<formulation ,Integer>("id"));
		colFormulationType.setCellValueFactory(new PropertyValueFactory<formulation,String>("formulationtype"));
		colStatus.setCellValueFactory(new PropertyValueFactory<formulation,String>("status"));
		colEdit.setCellFactory(cellFactory);
	}
	
	Callback<TableColumn<formulation, Boolean>, TableCell<formulation, Boolean>> cellFactory = 
			new Callback<TableColumn<formulation, Boolean>, TableCell<formulation, Boolean>>()
	{
		@Override
		public TableCell<formulation, Boolean> call( final TableColumn<formulation, Boolean> param)
		{
			final TableCell<formulation, Boolean> cell = new TableCell<formulation, Boolean>()
			{
				
				Image imgEdit = new Image(getClass().getResourceAsStream("/images/edit.png"));
				final Button btnEdit = new Button();
				
				@Override
				public void updateItem(Boolean check, boolean empty)
				{
					super.updateItem(check, empty);
					if(empty)
					{
						setGraphic(null);
						setText(null);
					}
					else{
						btnEdit.setOnAction(e ->{
							formulation fbean = getTableView().getItems().get(getIndex());
							updateUser(fbean);
						});
						
						btnEdit.setStyle("-fx-background-color: transparent;");
						ImageView iv = new ImageView();
				        iv.setImage(imgEdit);
				        iv.setPreserveRatio(true);
				        iv.setSmooth(true);
				        iv.setCache(true);
						btnEdit.setGraphic(iv);
						
						setGraphic(btnEdit);
						setAlignment(Pos.CENTER);
						setText(null);
					}
				}

				private void updateUser(formulation fbean) {
					formulationId.setText(Long.toString(fbean.getId()));
					formulationType.setText(fbean.getFormulationtype());
					status.getSelectionModel().select(fbean.getStatus());
				}
			};
			return cell;
		}
	};

	
	
	/*
	 *  Add All users to observable list and update table
	 */
	private void loadUserDetails(){
		userList.clear();
		userList.addAll(employeeService.listFormulation());

		userTable.setItems(userList);
	}
	
	/*
	 * Validations
	 */
	private boolean validate(String field, String value, String pattern){
		if(!value.isEmpty()){
			Pattern p = Pattern.compile(pattern);
	        Matcher m = p.matcher(value);
	        if(m.find() && m.group().equals(value)){
	            return true;
	        }else{
	        	validationAlert(field, false);            
	            return false;            
	        }
		}else{
			validationAlert(field, true);            
            return false;
		}        
    }
	
	private boolean emptyValidation(String field, boolean empty){
        if(!empty){
            return true;
        }else{
        	validationAlert(field, true);            
            return false;            
        }
    }	
	
	private void validationAlert(String field, boolean empty){
		Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Validation Error");
        alert.setHeaderText(null);
        if(field.equals("Role")) alert.setContentText("Please Select "+ field);
        else{
        	if(empty) alert.setContentText("Please Enter "+ field);
        	else alert.setContentText("Please Enter Valid "+ field);
        }
        alert.showAndWait();
	}
}
