package com.dineshonjava.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class AdminHeader_Controller implements Initializable
{



	 @FXML
	    private Label UserName_Label;
	 private void loadUserDetails()
		{
			
			String UserName=User_LogIn.getUserName();
			System.out.println("UserName At Admin Home..="+UserName);
			UserName_Label.setText("WELCOME "+UserName);
			
			
		}
		@Override
		public void initialize(URL location, ResourceBundle resources)
		{
		
			loadUserDetails();
		}
		@FXML
		public void addChiefManager(ActionEvent event)throws Exception 
		{
			//((Node) event.getSource()).getScene().getWindow().hide();
			Stage stage =new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/AddChiefInfo.fxml"));
			
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			stage.setScene(scene);
		    stage.show();
		}
		@FXML
		public void addAdmin(ActionEvent event)throws Exception 
		{
			//((Node) event.getSource()).getScene().getWindow().hide();
			Stage stage =new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/User.fxml"));
			
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			stage.setScene(scene);
		    stage.show();
		}
		@FXML
		public void addExecutiveManager(ActionEvent event)throws Exception 
		{
			//((Node) event.getSource()).getScene().getWindow().hide();
			Stage stage =new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/AddExecutiveInfo.fxml"));
			
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			stage.setScene(scene);
		    stage.show();
		}
		@FXML
		public void admin_changepassword(ActionEvent event)throws Exception 
		{
			//((Node) event.getSource()).getScene().getWindow().hide();
			Stage stage =new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/ChangePassword.fxml"));
			
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			stage.setScene(scene);
		    stage.show();
		}
		@FXML
		 private void logout(ActionEvent event) throws IOException 
		{
			((Node) event.getSource()).getScene().getWindow().hide();
			Stage stage =new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Main_Login.fxml"));
			
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			stage.setScene(scene);
		    stage.show(); 
		}
}