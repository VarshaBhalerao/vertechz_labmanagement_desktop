package com.dineshonjava.controller;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;

import com.dineshonjava.model.Records;
import com.dineshonjava.model.formulation;
import com.dineshonjava.service.EmployeeService;
import com.dineshonjava.service.EmployeeServiceImpl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class SearchController  implements   Initializable{
	
	/*@FXML
	private Label dateID;*/
	@FXML
	private DatePicker EnDateID;
	
	@FXML
	private   DatePicker EnterDate2;
	
	@FXML
	private TableView<Records> userTable;
    
	
	@FXML
	private TableColumn<Records, Integer> ColumnId;
	
	@FXML
	private TableColumn<Records, LocalDate> columnDate;
	
	@FXML
	private TableColumn<Records, String> ReseachExeColumn;
	
	@FXML
	private TableColumn<Records, String> approvedColumn;
	@FXML
	private TableColumn<Records, String> ObjectiveColumn;
	
	@FXML
	private TableColumn<Records, LocalDate> ReminderDateColumn;
	
	@FXML
	private TableColumn<Records, String> Formucolumn;
	
	@FXML
	private TableColumn<Records, String> ProcessColumn;
	
	@FXML
	private TableColumn<Records, String> ObsevationColumn;
	
	@FXML
	private TableColumn<Records, String> statusColumn;
	
	@FXML
	private TableColumn<Records, String> activeColumn;
	
	@FXML
	private TableColumn<Records, String> RefColumn;
	
	
	private EmployeeService employeeService = new EmployeeServiceImpl();
	/*
	public ObservableList<Records> data;*/
	
	private ObservableList<Records> userList = FXCollections.observableArrayList();
	

	
	public void SearchRecord(ActionEvent event) throws Exception{
		
		  Records records = new Records();
		  
		  //records.setDate(EnDateID.getValue());
		  //records.setDate(EnterDate2.getValue()); 
		  LocalDate date1 = EnDateID.getValue();
		  LocalDate date2 = EnterDate2.getValue();
		 
		  //LocalDate date1 =records.getDate();
		  //LocalDate   date2 = records.getDate();
	 	
		   /*try {
			   if(!(date1.equals("")) && !(date2.equals(""))) {
				   employeeService.SerchBySingleDate(date1, date2);
			   }
			   
			   else if(!(date1.equals("")) && date2.equals("")) {
				   employeeService.SerchByDate(date1,date2);
			   }
			   
			   else if((date1.equals("")) && date2.equals(""))
			
			   employeeService.SerchByDate1(date1, date2);
			   
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} 
	*/	    System.out.println("EnDateID.getValue()"+EnDateID.getValue());
		    System.out.println("EnterDate2.getValue()"+EnterDate2.getValue());
		    employeeService.SerchByDate1(date1, date2);
		    //initializeTable(date1, date2);
		    userTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			
			setColumnProperties();
			
			// Add all users into table
			loadUserDetails(date1, date2);
	}
	
	/*@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
	}*/
	
	/* private List<Records> initializeTable(LocalDate d1,LocalDate d2) {

	        data = FXCollections.observableArrayList();

	       data= (ObservableList<Records>) employeeService.SerchByDate1(d1, d2);

	        return data;

	    }*/

private void setColumnProperties(){
		// TODO Auto-generated method stub
		ColumnId.setCellValueFactory(new PropertyValueFactory<Records, Integer>("rid"));
		columnDate.setCellValueFactory(new PropertyValueFactory<Records, LocalDate>("date1"));  
		ReminderDateColumn.setCellValueFactory(new PropertyValueFactory<Records, LocalDate>("reminder_date1"));  
		ReseachExeColumn.setCellValueFactory(new PropertyValueFactory<Records, String>("research_executive"));
		approvedColumn.setCellValueFactory(new PropertyValueFactory<Records, String>("approved_by"));
		Formucolumn.setCellValueFactory(new PropertyValueFactory<Records, String>("formulation"));
		ObjectiveColumn.setCellValueFactory(new PropertyValueFactory<Records, String>("objective"));
		ProcessColumn.setCellValueFactory(new PropertyValueFactory<Records, String>("process_details"));
		statusColumn.setCellValueFactory(new PropertyValueFactory<Records, String>("status"));
		ObsevationColumn.setCellValueFactory(new PropertyValueFactory<Records, String>("Observations"));
		activeColumn.setCellValueFactory(new PropertyValueFactory<Records, String>("records_active"));
		RefColumn.setCellValueFactory(new PropertyValueFactory<Records, String>("referance_no"));
         		
		
	}
private void loadUserDetails(LocalDate date1, LocalDate date2){
	userList.clear();
	userList.addAll(employeeService.SerchByDate1(date1, date2));

	userTable.setItems(userList);
}

@Override
public void initialize(URL arg0, ResourceBundle arg1) {
	// TODO Auto-generated method stub
	//userList=(ObservableList<Records>) employeeService.listRecords();
}


		
                                    
}
