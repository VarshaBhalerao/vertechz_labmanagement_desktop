package com.dineshonjava.controller;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.dineshonjava.model.chief_login;
import com.dineshonjava.service.EmployeeService;
import com.dineshonjava.service.EmployeeServiceImpl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class LoginController 
{
	private EmployeeService employeeService = new EmployeeServiceImpl();
@FXML
private TextField username_login;
@FXML
private TextField password_login;

public String getUserName()
{
	return username_login.getText();
}
public String getPassword()
{
	return password_login.getText();
}

@SuppressWarnings("rawtypes")
public void Submit_Login(ActionEvent event)throws Exception
{
	 if( validate("password", getPassword(),"[a-zA-Z0-9]+"))
			 {
		 
	 
		List<chief_login> list = employeeService.listChief();
		if(list.isEmpty())
		{
			System.out.println("List is Empty...");
		}
		System.out.println("After List");
		int i =0;
		String Designation="Chief Manager";
		String Designation1="Executive Manager";
		String Designation2="Admin";
		System.out.println(list);
		System.out.println("Before For:");
		for(chief_login bean:list)
		{
			System.out.println("in for...." + ++i);
			if(!(getUserName().isEmpty()&& getPassword().isEmpty()))
			{
					if(getUserName().equals(bean.getUserName()) && getPassword().equals(bean.getPassword()) && bean.getDesignation().equalsIgnoreCase(Designation))
					{
System.out.println("Login Sucessfully");
						
						((Node) event.getSource()).getScene().getWindow().hide();
						Stage stage =new Stage();
						User_LogIn.setUserName(getUserName());
						String UserName=User_LogIn.getUserName();
						System.out.println("UserName After Login..="+UserName);
						Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/ATSRemendar.fxml"));
						
						Scene scene = new Scene(root);
						//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
						stage.setScene(scene);
					    stage.show();

					}
					if(getUserName().equals(bean.getUserName()) && getPassword().equals(bean.getPassword()) && bean.getDesignation().equalsIgnoreCase(Designation1))
					{
						System.out.println("Login Sucessfully");
						((Node) event.getSource()).getScene().getWindow().hide();
						User_LogIn.setUserName(getUserName());
						String UserName=User_LogIn.getUserName();
						System.out.println("UserName After Login..="+UserName);
						Stage stage =new Stage();
						Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Executive_ATSRemendar.fxml"));
						
						Scene scene = new Scene(root);
						//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
						stage.setScene(scene);
					    stage.show();
					}
					if(getUserName().equals(bean.getUserName()) && getPassword().equals(bean.getPassword()) && bean.getDesignation().equalsIgnoreCase(Designation2))
					{
						System.out.println("Login Sucessfully");
						((Node) event.getSource()).getScene().getWindow().hide();
						User_LogIn.setUserName(getUserName());
						String UserName=User_LogIn.getUserName();
						System.out.println("UserName After Login..="+UserName);
						Stage stage =new Stage();
						Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/ADMIN_HOME.fxml"));
						
						Scene scene = new Scene(root);
						//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
						stage.setScene(scene);
					    stage.show();
					}
			    }
				else
				{
					 Alert alert = new Alert(AlertType.WARNING);
				        alert.setTitle("Validation Error");
				        alert.setHeaderText(null);
				        alert.setContentText("Enter Valid UserName and Password");
				}
			}
		 }
	 
      
	}
	@FXML
	public void CANCEL_Login(ActionEvent event1)throws Exception
	{
		password_login.clear();
		username_login.clear();
		
	}
	@FXML
	public void ForgotPassword(ActionEvent event)throws Exception
	{
		((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage=new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/ForgotPassword.fxml"));
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		


	}
	
	@FXML
	public void NEW_USER_ADMIN(ActionEvent event)throws Exception
	{
		((Node) event.getSource()).getScene().getWindow().hide();
			Stage primaryStage=new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/User.fxml"));
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		


	}
	
	/*
	 * Validations
	 */
	private boolean validate(String field, String value, String pattern){
		if(!value.isEmpty()){
			Pattern p = Pattern.compile(pattern);
	        Matcher m = p.matcher(value);
	        if(m.find() && m.group().equals(value)){
	            return true;
	        }else{
	        	validationAlert(field, false);            
	            return false;            
	        }
		}else{
			validationAlert(field, true);            
            return false;
		}        
    }
	
	private boolean emptyValidation(String field, boolean empty){
        if(!empty){
            return true;
        }else{
        	validationAlert(field, true);            
            return false;            
        }
    }	
	
	private void validationAlert(String field, boolean empty){
		Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Validation Error");
        alert.setHeaderText(null);
        if(field.equals("Role")) alert.setContentText("Please Select "+ field);
        else{
        	if(empty) alert.setContentText("Please Enter "+ field);
        	else alert.setContentText("Please Enter Valid "+ field);
        }
        alert.showAndWait();
	}
			 

}
