package com.dineshonjava.controller;

import java.io.IOException;

import java.util.List;
import java.util.Optional;

import java.util.regex.Matcher;
import java.util.regex.Pattern;



import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;
import com.dineshonjava.model.*;
import com.dineshonjava.service.EmployeeService;
import com.dineshonjava.service.EmployeeServiceImpl;

public class AtsController {

	private EmployeeService employeeService = new EmployeeServiceImpl();
	@FXML
    private Button btnLogout;
	
	@FXML
    private Label atsId;
	
	@FXML
    private TextField days_description;
	
	@FXML
	private ComboBox<String> status;
	
	@FXML
	private TableView<ats> userTable;

	@FXML
	private TableColumn<ats, Integer> colAtsId;

	@FXML
	private TableColumn<ats, String> colDaysDescription;

	@FXML
	private TableColumn<ats, String> colStatus;

	@FXML
    private TableColumn<ats, Boolean> colEdit;
	
	@FXML
    private MenuItem deleteUsers;
	
	/*@Lazy
    @Autowired
    private StageManager stageManager;
	
	@Autowired
	private UserService userService;
	*/
	private ObservableList<ats> userList = FXCollections.observableArrayList();
	private ObservableList<String> roles = FXCollections.observableArrayList("Enabled", "Disabled");
	
	@FXML
	private void exit(ActionEvent event) {
		Platform.exit();
    }

	/**
	 * Logout and go to the login page
	 */
    @FXML
    private void logout(ActionEvent event) throws IOException {
    	//stageManager.switchScene(FxmlView.LOGIN);    	
    }
    
    @FXML
    void reset(ActionEvent event) {
    	clearFields();
    }
    
	
	
	@FXML
    private void saveUser(ActionEvent event){
    	
    	if( validate("Days Description", getDaysDescription(), "^[\\p{L} .'-]+$") &&   	    emptyValidation("Status", getStatus() == null) ){
    		
    		if(atsId.getText() == null || atsId.getText() == ""){
    			
    				/*User user = new User();
        			user.setformulationType(getformulationType());
        			user.setLastName(getLastName());
        			user.setDob(getDob());
        			user.setGender(getGender());
        			user.setRole(getRole());
        			user.setEmail(getEmail());
        			user.setPassword(getPassword());
        			
        			User newUser = userService.save(user);*/
    			
    				ats fbean = new ats();
    			    fbean.setDays_description(getDaysDescription());
    			    fbean.setStatus(getStatus());
    			    employeeService.addATS(fbean);
        			
        			saveAlert(fbean);
    			
    			
    		}else{
    			ats fbean = employeeService.getATS(Integer.parseInt(atsId.getText()));
    			fbean.setDays_description(getDaysDescription());
    			fbean.setStatus(getStatus());
    			 employeeService.addATS(fbean);
    			
    			updateAlert(fbean);
    		}
    		
    		clearFields();
    		loadUserDetails();
    	}
    	
    	
    }
    
    @FXML
    private void deleteUsers(ActionEvent event){
    	List<ats> atsList= userTable.getSelectionModel().getSelectedItems();
    	
    	Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText(null);
		alert.setContentText("Are you sure you want to delete selected?");
		Optional<ButtonType> action = alert.showAndWait();
		
	if(action.get() == ButtonType.OK)
	{
		for(ats atsBean:atsList)
		{
			employeeService.deleteATS(atsBean);
		}
	}
    	
    	loadUserDetails();
    }
    
   	private void clearFields() {
		atsId.setText(null);
		days_description.clear();		
		status.getSelectionModel().clearSelection();
		
	}
	
	private void saveAlert(ats fbean){
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("User saved successfully.");
		alert.setHeaderText(null);
		alert.setContentText("The user "+fbean.getDays_description()+" "+" has been created and  id is "+ fbean.getId() +".");
		alert.showAndWait();
	}
	
	private void updateAlert(ats fbean){
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("User updated successfully.");
		alert.setHeaderText(null);
		alert.setContentText("The user "+fbean.getDays_description()+" has been updated.");
		alert.showAndWait();
	}
	
	

	public String getDaysDescription() {
		return days_description.getText();
	}

	
	
	public String getStatus() {
		return status.getSelectionModel().getSelectedItem();
	}

	
  

	public void initialize() {
		
		status.setItems(roles);
		
		userTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		setColumnProperties();
		
		// Add all users into table
		loadUserDetails();
	}
	
	
	
	/*
	 *  Set All userTable column properties
	 */
	private void setColumnProperties(){
		/* Override date format in table
		 * colDOB.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<LocalDate>() {
			 String pattern = "dd/MM/yyyy";
			 DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
		     @Override 
		     public String toString(LocalDate date) {
		         if (date != null) {
		             return dateFormatter.format(date);
		         } else {
		             return "";
		         }
		     }

		     @Override 
		     public LocalDate fromString(String string) {
		         if (string != null && !string.isEmpty()) {
		             return LocalDate.parse(string, dateFormatter);
		         } else {
		             return null;
		         }
		     }
		 }));*/
		
		colAtsId.setCellValueFactory(new PropertyValueFactory<ats ,Integer>("id"));
		colDaysDescription.setCellValueFactory(new PropertyValueFactory<ats,String>("days_description"));
		colStatus.setCellValueFactory(new PropertyValueFactory<ats,String>("status"));
		colEdit.setCellFactory(cellFactory);
	}
	
	Callback<TableColumn<ats, Boolean>, TableCell<ats, Boolean>> cellFactory = 
			new Callback<TableColumn<ats, Boolean>, TableCell<ats, Boolean>>()
	{
		@Override
		public TableCell<ats, Boolean> call( final TableColumn<ats, Boolean> param)
		{
			final TableCell<ats, Boolean> cell = new TableCell<ats, Boolean>()
			{
				
				Image imgEdit = new Image(getClass().getResourceAsStream("/images/edit.png"));
				final Button btnEdit = new Button();
				
				@Override
				public void updateItem(Boolean check, boolean empty)
				{
					super.updateItem(check, empty);
					if(empty)
					{
						setGraphic(null);
						setText(null);
					}
					else{
						btnEdit.setOnAction(e ->{
							ats fbean = getTableView().getItems().get(getIndex());
							updateUser(fbean);
						});
						
						btnEdit.setStyle("-fx-background-color: transparent;");
						ImageView iv = new ImageView();
				        iv.setImage(imgEdit);
				        iv.setPreserveRatio(true);
				        iv.setSmooth(true);
				        iv.setCache(true);
						btnEdit.setGraphic(iv);
						
						setGraphic(btnEdit);
						setAlignment(Pos.CENTER);
						setText(null);
					}
				}

				private void updateUser(ats fbean) {
					atsId.setText(Long.toString(fbean.getId()));
					days_description.setText(fbean.getDays_description());
					status.getSelectionModel().select(fbean.getStatus());
				}
			};
			return cell;
		}
	};

	
	
	/*
	 *  Add All users to observable list and update table
	 */
	private void loadUserDetails(){
		userList.clear();
		userList.addAll(employeeService.listATS());

		userTable.setItems(userList);
	}
	
	/*
	 * Validations
	 */
	private boolean validate(String field, String value, String pattern){
		if(!value.isEmpty()){
			Pattern p = Pattern.compile(pattern);
	        Matcher m = p.matcher(value);
	        if(m.find() && m.group().equals(value)){
	            return true;
	        }else{
	        	validationAlert(field, false);            
	            return false;            
	        }
		}else{
			validationAlert(field, true);            
            return false;
		}        
    }
	
	private boolean emptyValidation(String field, boolean empty){
        if(!empty){
            return true;
        }else{
        	validationAlert(field, true);            
            return false;            
        }
    }	
	
	private void validationAlert(String field, boolean empty){
		Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Validation Error");
        alert.setHeaderText(null);
        if(field.equals("Role")) alert.setContentText("Please Select "+ field);
        else{
        	if(empty) alert.setContentText("Please Enter "+ field);
        	else alert.setContentText("Please Enter Valid "+ field);
        }
        alert.showAndWait();
	}
}

