package com.dineshonjava.controller;

import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.ResourceBundle;

import com.dineshonjava.model.Records;
import com.dineshonjava.service.EmployeeService;
import com.dineshonjava.service.EmployeeServiceImpl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.sql.*;

public class Chief_Record_Entry_NextController implements Initializable {
	
	
	@FXML
	private Label recordId;
	@FXML
	private TextField ref_no;
	@FXML
	private DatePicker date;
	@FXML
	private TextField formulation;
	@FXML
	private TextField research_executive;
	@FXML
	private TextField aprroved_by;
	@FXML
	private TextField objective;
	@FXML
	private TextArea process_details;
	@FXML
	private TextArea observations;
	@FXML
	private DatePicker reminder_date;
	@FXML
	private TextField status;
	
	private EmployeeService employeeService = new EmployeeServiceImpl();
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		
		SetElements();
		
	}


	private void SetElements()
	{
		int rid=User_LogIn.getRid();
		System.out.println("Rid After Login..="+rid);
		
		Records records = employeeService.getRecords(rid);
		/*System.out.println("rid: "+records.getRid());
		System.out.println("getDate(): "+records.getDate());
		System.out.println("getReminder_date(): "+records.getReminder_date());
		System.out.println("getResearch_executive(): "+records.getResearch_executive());
		System.out.println("getObjective(): "+records.getObjective());
		System.out.println("getStatus(): "+records.getStatus());
		System.out.println("getObservations(): "+records.getObservations());
		System.out.println("getProcess_details(): "+records.getProcess_details());
		System.out.println("getApproved_by(): "+records.getApproved_by());
		System.out.println("getReferance_no(): "+records.getReferance_no());
		*/
		
		recordId.setText(Long.toString(records.getRid()));
		
		String ref_no1=records.getDate().toString();
		
		System.out.println("Reference no is"+ref_no1);
		
		
		LocalDate date1 =records.getDate();
	    /*DateTimeFormatter formatters = DateTimeFormatter.ofPattern("uuuu/MM/d");
	    String text = date1.format(formatters);
	    LocalDate parsedDate = LocalDate.parse(text, formatters);

	    System.out.println("date: " + date1); // date: 2016-09-25
	    System.out.println("Text format " + text); // Text format 25/09/2016
	    System.out.println("parsedDate: " + parsedDate);
	    
	    try
	    {
	    Class.forName("com.mysql.jdbc.Driver");
    	Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/desktop_lab_management", "root", "root");
        //  alert("Pramod");
    	Statement st = con.createStatement();
    	String sql = ("SELECT * FROM records where date='"+date1+"'");
    	System.out.println("SQL Query is"+sql);
    	ResultSet rs = st.executeQuery(sql);
    	
    	int count=1;
    	while(rs.next()) {
    		count++;
    		
    		
    	}
    	System.out.println("Count is"+count);
	    }
	    
	    catch(Exception e)
	    {
	    	
	    }
*/	    
		ref_no.setText(records.getReferance_no());
		formulation.setText(records.getFormulation());
		date.setValue(records.getDate());
		research_executive.setText(records.getResearch_executive());
		objective.setText(records.getObjective());
		status.setText(records.getStatus());
		observations.setText(records.getObservations());
		reminder_date.setValue(records.getReminder_date());
		aprroved_by.setText(records.getApproved_by());
		process_details.setText(records.getProcess_details());
		
	}
	
	 @FXML
		public void Active(ActionEvent event)throws Exception
		{
			
			
			Stage primaryStage=new Stage();
			primaryStage.close();
				
			
				Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Active.fxml"));
				Scene scene = new Scene(root);
				//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.show();
		}
	 
	 @FXML
		public void Selflife_Reminders(ActionEvent event)throws Exception
		{
			
			
			Stage primaryStage=new Stage();
			primaryStage.close();
				
			
				Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Selflife_Reminders.fxml"));
				Scene scene = new Scene(root);
				//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.show();
		}

	 @FXML
		public void CompositionDetails(ActionEvent event)throws Exception
		{
			
			
			Stage primaryStage=new Stage();
			primaryStage.close();
				
			
				Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/CompositionDetails.fxml"));
				Scene scene = new Scene(root);
				//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.show();
		}

}
