package com.dineshonjava.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;


import com.dineshonjava.model.Records;
import com.dineshonjava.model.chief_login;
import com.dineshonjava.model.formulation;
import com.dineshonjava.service.EmployeeService;
import com.dineshonjava.service.EmployeeServiceImpl;
import com.sun.prism.impl.Disposer.Record;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Callback;

public class RecordController implements Initializable{
	
	@FXML
	private Label recordId;
	@FXML
	private DatePicker date;
	@FXML
	private TextField referance_no;
	@FXML
	private TextField objective;
	@FXML
	private TextArea process_details;
	@FXML
	private TextArea observations;
	@FXML
	private DatePicker reminder_date;

	@FXML
	private TableView<Records> recordTable;

	@FXML
	private TableColumn<Records, LocalDate> coldate;

	@FXML
	private TableColumn<Records, String> colrefno;

	@FXML
	private TableColumn<Records, String> colobjective;

	@FXML
	private TableColumn<Records, String> colformulation;

	@FXML
	private TableColumn<Records, String> colresearchexecutive;
	
	@FXML
    private TableColumn<Records, String> colstatus;

	@FXML
	private TableColumn<Records, Boolean> coledit;
	
	@FXML
    private TableColumn<Records, Boolean> coldelete;
	

	
	 @FXML
	 private ComboBox<String> research_executive; 
	 @FXML
	 private ComboBox<String> approved_by;
	 @FXML
	 private ComboBox<String> status;
	 @FXML
	 private ComboBox<String> formulation;
	 private EmployeeService employeeService = new EmployeeServiceImpl();
	 private ObservableList<Records> recordList = FXCollections.observableArrayList();
	 private ObservableList<String> research_executive1 = FXCollections.observableArrayList("In Process", "Finalise", "Rejected");
	 private ObservableList<String> status1 = FXCollections.observableArrayList("In Process", "Finalise", "Rejected");
	 private ObservableList<String> approved_by1 = FXCollections.observableArrayList("Apple", "Orange", "Pear");
	private ObservableList<String> formulation1 = FXCollections.observableArrayList("Apple", "Orange", "Pear");
	 //private ObservableList<String> formulation1;
	 
	 @FXML
		private void exit(ActionEvent event) {
			Platform.exit();
	    }
	 @FXML
	    void reset(ActionEvent event) {
	    	clearFields();
	    }
		
		@SuppressWarnings("unchecked")
		@Override
		public void initialize(URL location, ResourceBundle resources) {
		     ArrayList formulatiolist = new ArrayList();
			List list = employeeService.listFormulation();
			for(Object obj : list){
				formulation flist= (formulation)obj;
				formulatiolist.add(flist.getFormulationtype() );
			}
			 ArrayList executivelist = new ArrayList();
				List list1 = employeeService.listChief();
				for(Object obj : list1){
					chief_login elist= (chief_login)obj;
					executivelist.add(elist.getUserName() );
				}
				
				 ArrayList managerlist = new ArrayList();
					List list2 = employeeService.getEmployeeList("Chief Manager");
					for(Object obj : list2){
						chief_login mlist= (chief_login)obj;
     						managerlist.add(mlist.getUserName() );
					}
			
			
			
			research_executive.getItems().setAll(executivelist);
			approved_by.getItems().setAll(managerlist);
			status.setItems(status1);
			//formulation.setItems(formulation1);
			formulation.getItems().setAll(formulatiolist);
			//recordTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			
			//setColumnProperties();
			
			// Add all users into table
			//loadUserDetails();
		}

     private void loadUserDetails() {
    	
    	 recordList.clear();
    	 recordList.addAll(employeeService.listRecords());

 		recordTable.setItems(recordList);
			
		}
    
	private void setColumnProperties() {
		/*coldate.setCellValueFactory(new PropertyValueFactory<Records, LocalDate>("date"));
		colformulation.setCellValueFactory(new PropertyValueFactory<Records, String>("formulation"));
		colresearchexecutive.setCellValueFactory(new PropertyValueFactory<Records, String>("research_executive"));
		colobjective.setCellValueFactory(new PropertyValueFactory<Records, String>("objective"));
		colrefno.setCellValueFactory(new PropertyValueFactory<Records, String>("referance_no"));
		
		colstatus.setCellValueFactory(new PropertyValueFactory<Records, String>("status"));
		coledit.setCellFactory(cellFactory);
		coldelete.setCellFactory(cellFactory);*/
			
		}
	Callback<TableColumn<Records, Boolean>, TableCell<Records, Boolean>> cellFactory = 
			new Callback<TableColumn<Records, Boolean>, TableCell<Records, Boolean>>()
	{
		@Override
		public TableCell<Records, Boolean> call( final TableColumn<Records, Boolean> param)
		{
			final TableCell<Records, Boolean> cell = new TableCell<Records, Boolean>()
			{
				Image imgEdit = new Image(getClass().getResourceAsStream("/images/edit.png"));
				final Button btnEdit = new Button();
				
				@Override
				public void updateItem(Boolean check, boolean empty)
				{
					super.updateItem(check, empty);
					if(empty)
					{
						setGraphic(null);
						setText(null);
					}
					else{
						btnEdit.setOnAction(e ->{
							Records records = getTableView().getItems().get(getIndex());
							updateUser(records);
						});
						
						btnEdit.setStyle("-fx-background-color: transparent;");
						ImageView iv = new ImageView();
				        iv.setImage(imgEdit);
				        iv.setPreserveRatio(true);
				        iv.setSmooth(true);
				        iv.setCache(true);
						btnEdit.setGraphic(iv);
						
						setGraphic(btnEdit);
						setAlignment(Pos.CENTER);
						setText(null);
					}
				}

				private void updateUser(Records records) {
					//userId.setText(Long.toString(user.getId()));
					recordId.setText(Long.toString(records.getRid()));
					referance_no.setText(records.getReferance_no());
					formulation.getSelectionModel().select(records.getFormulation());
					date.setValue(records.getDate());
					research_executive.getSelectionModel().select(records.getResearch_executive());
					objective.setText(records.getObjective());
					status.getSelectionModel().select(records.getStatus());
				}
			};
			return cell;
		}
	};


	public void SaveRecord(ActionEvent event) throws Exception{
		
		if(recordId.getText() == null || recordId.getText() == ""){
	    System.out.println("date = "+date.getValue());
	    System.out.println("reminder_date = "+reminder_date.getValue());
	   /* SimpleDateFormat s =new SimpleDateFormat("yyyy/MM/dd");
	    List list = employeeService.SerchBySingleDate(date.getValue());
	    for(Object o:list){
	    	Records records = (Records)o;
	    	System.out.println(records.getDate());
	    }*/
		//String ref_no="VER"+"/"+s.format(date.getValue())+"/"+count;
 	    Records records = new Records();
 	    records.setDate(date.getValue());
 	    records.setFormulation(formulation.getSelectionModel().getSelectedItem());
 	    records.setApproved_by(approved_by.getSelectionModel().getSelectedItem());
 	    records.setResearch_executive(research_executive.getSelectionModel().getSelectedItem());
 	    records.setStatus(status.getSelectionModel().getSelectedItem());
 	    records.setObservations(observations.getText());
 	    records.setReminder_date(reminder_date.getValue());
 	    records.setProcess_details(process_details.getText());
 	    records.setObjective(objective.getText());
 	    
 	   LocalDate date1 =records.getDate();
	 	  DateTimeFormatter formatters = DateTimeFormatter.ofPattern("uuuu/MM/d");
		    String text = date1.format(formatters);
		    LocalDate parsedDate = LocalDate.parse(text, formatters);
		    
		    LocalDate date2=records.getDate();
		    LocalDate date3=records.getReminder_date();
		    DateTimeFormatter formatters1 = DateTimeFormatter.ofPattern("uuuu-MM-dd");
		    String text1 = date2.format(formatters1);
		    String text2 = date3.format(formatters1);
		    

		    System.out.println("date: " + date1); // date: 2016-09-25
		    System.out.println("Text format " + text); // Text format 25/09/2016
		    System.out.println("parsedDate: " + parsedDate);
		    int count=1;
		    try
		    {
		    Class.forName("com.mysql.jdbc.Driver");
	    	Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/desktop_lab_management", "root", "root");
	        //  alert("Pramod");
	    	Statement st = con.createStatement();
	    	String sql = ("SELECT * FROM records where referance_no like'VER/"+text+"/%'");
	    	System.out.println("SQL Query is"+sql);
	    	ResultSet rs = st.executeQuery(sql);
	    	
	    	
	    	while(rs.next()) {
	    		count++;
	    		
	    		
	    	}
	    	System.out.println("Count is"+count);
		   
         
 }
		    
		    catch(Exception e)
		    {
		    	
		    }
		 records.setReferance_no("VER/"+text+"/"+count);
		 records.setDate1(text1);
		 records.setReminder_date1(text2);
 	    employeeService.addRecords(records);
 	    
 	    User_LogIn.setRid(records.getRid());
		int rid=User_LogIn.getRid();
		System.out.println("rid in SaveRecord..="+rid);
 	    
 		Stage primaryStage=new Stage();
		primaryStage.close();
			
		
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Chief_Record_Entry_Next.fxml"));
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
 	    
 	   saveAlert(records);
		}else{
			
			Records records = employeeService.getRecords(Integer.parseInt(recordId.getText()));
			 records.setDate(date.getValue());
		 	    records.setFormulation(records.getFormulation());
		 	    records.setApproved_by(records.getApproved_by());
		 	    records.setResearch_executive(records.getResearch_executive());
		 	    records.setStatus(records.getStatus());
		 	    records.setObservations(records.getObservations());
		 	    records.setReminder_date(records.getReminder_date());
		 	    records.setProcess_details(records.getProcess_details());
		 	    records.setObjective(records.getObjective());
		 	    
		 	 		 	   
			 employeeService.addRecords(records);
			 
			 User_LogIn.setRid(records.getRid());
				int rid=User_LogIn.getRid();
				System.out.println("UserName After Login..="+rid);
			
			updateAlert(records);
			
		}
		
		clearFields();
		loadUserDetails();
 	      
	}
	private void saveAlert(Records records){
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("User saved successfully.");
		alert.setHeaderText(null);
		alert.setContentText("The Record With Referance  Id "+records.getReferance_no()+" "+" has been created .");
		alert.showAndWait();
	}
	
	private void updateAlert(Records records){
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("User updated successfully.");
		alert.setHeaderText(null);
		alert.setContentText("The Record With Referance  Id "+records.getReferance_no()+" has been updated.");
		alert.showAndWait();
	}
	
	private void clearFields() {
		
		    date.setAccessibleText(null);
	 	    formulation.getSelectionModel().clearSelection();
	 	    approved_by.getSelectionModel().clearSelection();
	 	    research_executive.getSelectionModel().clearSelection();
	 	    status.getSelectionModel().clearSelection();
	 	    observations.clear();
	 	    reminder_date.setAccessibleText(null);
	 	    process_details.clear();
	 	    objective.clear();
		
		
	}
	
	 @FXML
	    private void deleteRecords(ActionEvent event){
	    	List<Records> recordList= recordTable.getSelectionModel().getSelectedItems();
	    	
	    	Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Confirmation Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Are you sure you want to delete selected?");
			Optional<ButtonType> action = alert.showAndWait();
			
		if(action.get() == ButtonType.OK)
		{
			for(Records recordBean : recordList)
			{
				employeeService.deleteRecord(recordBean);
			}
		}
	    	
	    	loadUserDetails();
	    }
	    

	
}

