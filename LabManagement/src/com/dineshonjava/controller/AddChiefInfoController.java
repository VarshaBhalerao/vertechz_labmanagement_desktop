package com.dineshonjava.controller;

	import java.io.IOException;
	import java.net.URL;
	import java.text.DateFormat;
	import java.text.SimpleDateFormat;
	import java.time.LocalDate;
	import java.util.Calendar;
	import java.util.List;
	import java.util.Optional;
	import java.util.Properties;
	import java.util.ResourceBundle;
	import java.util.regex.Matcher;
	import java.util.regex.Pattern;

	import javax.mail.Message;
	import javax.mail.MessagingException;
	import javax.mail.PasswordAuthentication;
	import javax.mail.Session;
	import javax.mail.Transport;
	import javax.mail.internet.InternetAddress;
	import javax.mail.internet.MimeMessage;

	import org.springframework.stereotype.Controller;

	import com.dineshonjava.model.chief_login;
	import com.dineshonjava.model.formulation;
	import com.dineshonjava.service.EmployeeService;
	import com.dineshonjava.service.EmployeeServiceImpl;

	import javafx.application.Platform;
	import javafx.collections.FXCollections;
	import javafx.collections.ObservableList;
	import javafx.event.ActionEvent;
	import javafx.fxml.FXML;
	import javafx.fxml.FXMLLoader;
	import javafx.fxml.Initializable;
	import javafx.geometry.Pos;
	import javafx.scene.Node;
	import javafx.scene.Parent;
	import javafx.scene.Scene;
	import javafx.scene.control.Alert;
	import javafx.scene.control.Alert.AlertType;
	import javafx.scene.control.Button;
	import javafx.scene.control.ButtonType;
	import javafx.scene.control.ComboBox;
	import javafx.scene.control.DatePicker;
	import javafx.scene.control.Label;
	import javafx.scene.control.MenuItem;
	import javafx.scene.control.PasswordField;
	import javafx.scene.control.RadioButton;
	import javafx.scene.control.SelectionMode;
	import javafx.scene.control.TableCell;
	import javafx.scene.control.TableColumn;
	import javafx.scene.control.TableView;

	import javafx.scene.control.TextField;
	import javafx.scene.control.ToggleGroup;
	import javafx.scene.control.cell.PropertyValueFactory;
	import javafx.scene.image.Image;
	import javafx.scene.image.ImageView;
	import javafx.scene.input.KeyEvent;
	import javafx.stage.Stage;
	import javafx.util.Callback;
	/**
	 * @author Ram Alapure
	 * @since 05-04-2017
	 */

	//@Controller
	public class AddChiefInfoController  implements Initializable{
		private EmployeeService employeeService = new EmployeeServiceImpl();
		@FXML
	    private Button btnLogout;
		@FXML
		private Label empId;
		
		@FXML
	    private TextField firstName;

	    @FXML
	    private TextField lastName;

	    
	    @FXML
	    private TextField emailId;

	    @FXML
	    private TextField password;
	    @FXML
	    private TextField userName;
	    
	    @FXML
	    private TextField address;
	    @FXML
	    private TextField designation;
	    @FXML
	    private TextField country;
	    @FXML
	    private TextField contactNo;
	    
		
		@FXML
		private TableView<chief_login> userTable;

		@FXML
		private TableColumn<chief_login, String> colEmpId;
		@FXML
		private TableColumn<chief_login, String> colFirstName;

		@FXML
		private TableColumn<chief_login, String> colLastName;

		
		@FXML
		private TableColumn<chief_login, String> colEmailId;
		
		@FXML
	    private TableColumn<chief_login, String> colUserName;

		@FXML
		private TableColumn<chief_login, String> colPassword;
		@FXML
		private TableColumn<chief_login, String> colDesignation;
		
		@FXML
		private TableColumn<chief_login, String> colAddress;
		@FXML
		private TableColumn<chief_login, String> colContactNo;
		@FXML
	    private TableColumn<chief_login, Boolean> colEdit;
		
		@FXML
	    private MenuItem deleteUsers;
		
		
		 

		
		
		
		private ObservableList<chief_login> userList = FXCollections.observableArrayList();
		@FXML
		private void exit(ActionEvent event) {
			Platform.exit();
	    }

		/**
		 * Logout and go to the login page
		 */
		
		
		
		
	    @FXML
	    private void logout(ActionEvent event) throws IOException 
	    {
	    	((Node) event.getSource()).getScene().getWindow().hide();
			Stage stage =new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Main_Login.fxml"));
			
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			stage.setScene(scene);
		    stage.show();  	
	    }
	    
	   
	    
	    
	    @FXML
	    void reset(ActionEvent event) {
	    	clearFields();
	    }

		public String getEmailId() {
			return emailId.getText();
		}

		public String getAddress1() {
			return address.getText();
		}

		public String getCountry() {
			return country.getText();
		}

		public String getContactNo() {
			return contactNo.getText();
		}

		public String getFirstName() {
			return firstName.getText();
		}

		public String getLastName() {
			return lastName.getText();
		}

		public String getUserName() {
			return userName.getText();
		}


		public String getPassword() {
			return password.getText();
		}
		public String getDesignation() {
			return designation.getText();
		}
	    @FXML
	    private void submit(ActionEvent event){
	    	
	    	try 
	    	{
	    	if(validate("First Name", getFirstName(), "[a-zA-Z]+") &&
	    	   validate("Last Name", getLastName(), "[a-zA-Z]+")&&validate("Email", getEmailId(), "[a-zA-Z0-9][a-zA-Z0-9._]*@[a-zA-Z0-9]+([.][a-zA-Z]+)+")&& emptyValidation("Password", getPassword().isEmpty() ))
	    	{
	    		
	    		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 		   
	    	    Calendar calobj = Calendar.getInstance();
	    	    System.out.println(df.format(calobj.getTime()));
	    		String regDate=df.format(calobj.getTime());
	    		
	    		String UserName=getUserName();
	    		String password=getPassword();
	    		String email1=getEmailId();
	    		System.out.println("registraion date="+regDate.toString());
	    		List<chief_login> list = employeeService.listChief();
	    		int i =0;
	    		
	    		chief_login bean=new chief_login();
	    		bean.setFname(getFirstName());
	    		bean.setLname(getLastName());
	    		bean.setEmailId(getEmailId());
	    		bean.setUserName(getUserName());
	    		bean.setPassword(getPassword());
	    		bean.setAddress(getAddress1());
	    		bean.setDesignation(getDesignation());
	    		bean.setContactNo(getContactNo());
	    		bean.setCountry(getCountry());
	    		try
	    		{
	    			bean.setRegDate(df.parse(regDate));
	    			
	    		 employeeService.addManager(bean);
	    		
	    		}
	    		catch(Exception ex)
	    		{
	    		
	    			//return new ModelAndView("NewManager_Error");
	           
	    		}
	     
	    		System.out.println("For Sending Email");
	    		
	    		
	    		
	    		String subject="UserName and password";
	    		
	    		String str ="Dear "+ UserName+",Your UserName is  "+UserName+" and password is ";
	    		String message1=password+" thank you";
	    		String message=str.concat(message1);
	    		//String str=message+message1;
	    		// prints debug info
	    		//String message ="Dear User,Your password is  reset and your new password is "+ password+". Thank you.";
	    		
	    		// prints debug info
	    		System.out.println("To: " + email1);
	    		System.out.println("Subject: " + subject);
	    		System.out.println("Message: " + message);
	    		
	    		
	    		EmailInfo info=new EmailInfo();

	    		Session session = Session.getInstance(info.mailproperties(),
	    		  new javax.mail.Authenticator() {
	    			protected PasswordAuthentication getPasswordAuthentication() {
	    				return new PasswordAuthentication(info.getmailFrom_username(), info.getmailFrom_password());
	    			}
	    		  });

	    		

	    			Message message_1 = new MimeMessage(session);
	    			message_1.setFrom(new InternetAddress("username"));
	    			message_1.setRecipients(Message.RecipientType.TO,
	    				InternetAddress.parse(getEmailId()));
	    			message_1.setSubject("UserName and Password Information");
	    			message_1.setText(message);

	    			Transport.send(message_1);

	    			System.out.println("Done");
	         
	    		
	    		
	    		
	    		
	    			}
	    			
	    		else
	    		{
	    			
	    			chief_login bean=employeeService.getEmp((Integer.parseInt(empId.getText())));
	    			//chief_login bean=new chief_login();
	        		bean.setFname(getFirstName());
	        		bean.setLname(getLastName());
	        		bean.setEmailId(getEmailId());
	        		bean.setUserName(getUserName());
	        		bean.setPassword(getPassword());
	        		bean.setAddress(getAddress1());
	        		bean.setDesignation(getDesignation());
	        		bean.setContactNo(getContactNo());
	        		bean.setCountry(getCountry());
	        		 employeeService.addManager(bean);
	         		
	    			
	    		}
	    		
	    		clearFields();
	    		loadUserDetails();
	    	} catch (MessagingException e) {
				throw new RuntimeException(e);
			}
	    	
	    	}
	    	
	    	
	    
	    
	    @FXML
	    private void deleteUsers(ActionEvent event){
	    	List<chief_login> users = userTable.getSelectionModel().getSelectedItems();
	    	
	    	Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Confirmation Dialog");
			alert.setHeaderText(null);
			alert.setContentText("Are you sure you want to delete selected?");
			Optional<ButtonType> action = alert.showAndWait();
			
			//if(action.get() == ButtonType.OK) userService.deleteInBatch(users);
			if(action.get() == ButtonType.OK)
			{
				for(chief_login Bean:userList )
				{
					employeeService.deleteEmp(Bean);
				}
			}

	    	
	    	loadUserDetails();
	    }
	    
	   	private void clearFields() {
			
			firstName.clear();
			lastName.clear();
			address.clear();
			userName.clear();
			emailId.clear();
			password.clear();
			designation.clear();
			country.clear();
			contactNo.clear();
		}
		
		private void saveAlert(chief_login user){
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("User saved successfully.");
			alert.setHeaderText(null);
			//alert.setContentText("The user "+user.getFname()+" "+user.getLastName() +" has been created and \n"+getGenderTitle(user.getGender())+" id is "+ user.getId() +".");
			alert.showAndWait();
		}
		
		private void updateAlert(chief_login user){
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("User updated successfully.");
			alert.setHeaderText(null);
			alert.setContentText("The user "+user.getFname()+" "+user.getLname() +" has been updated.");
			alert.showAndWait();
		}
		
		
		
		

		
	  

		@Override
		public void initialize(URL location, ResourceBundle resources) {
			
		
			userTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			
			setColumnProperties();
			
			// Add all users into table
			loadUserDetails();
		}
		
		
		
		/*
		 *  Set All userTable column properties
		 */
		private void setColumnProperties()
		{
			
			colEmpId.setCellValueFactory(new PropertyValueFactory<>("id"));
			 colFirstName.setCellValueFactory(new PropertyValueFactory<>("fname"));
			 colLastName.setCellValueFactory(new PropertyValueFactory<>("lname"));
			 colEmailId.setCellValueFactory(new PropertyValueFactory<>("emailId"));
			 
			 colUserName.setCellValueFactory(new PropertyValueFactory<>("userName"));
			 colPassword.setCellValueFactory(new PropertyValueFactory<>("password"));
			 colAddress.setCellValueFactory(new PropertyValueFactory<>("address"));
			 colDesignation.setCellValueFactory(new PropertyValueFactory<>("designation"));
			 colContactNo.setCellValueFactory(new PropertyValueFactory<>("contactNo"));
			
			colEdit.setCellFactory(cellFactory);
		}
		
		Callback<TableColumn<chief_login, Boolean>, TableCell<chief_login, Boolean>> cellFactory = 
				new Callback<TableColumn<chief_login, Boolean>, TableCell<chief_login, Boolean>>()
		{
			@Override
			public TableCell<chief_login, Boolean> call( final TableColumn<chief_login, Boolean> param)
			{
				final TableCell<chief_login, Boolean> cell = new TableCell<chief_login, Boolean>()
				{
					Image imgEdit = new Image(getClass().getResourceAsStream("/images/edit.png"));
					final Button btnEdit = new Button();
					
					@Override
					public void updateItem(Boolean check, boolean empty)
					{
						super.updateItem(check, empty);
						if(empty)
						{
							setGraphic(null);
							setText(null);
						}
						else{
							btnEdit.setOnAction(e ->{
								chief_login user = getTableView().getItems().get(getIndex());
								updateUser(user);
							});
							
							btnEdit.setStyle("-fx-background-color: transparent;");
							ImageView iv = new ImageView();
					        iv.setImage(imgEdit);
					        iv.setPreserveRatio(true);
					        iv.setSmooth(true);
					        iv.setCache(true);
							btnEdit.setGraphic(iv);
							
							setGraphic(btnEdit);
							setAlignment(Pos.CENTER);
							setText(null);
						}
					}

					private void updateUser(chief_login user) {
						
						firstName.setText(user.getFname());
						lastName.setText(user.getLname());
						emailId.setText(user.getEmailId());
						userName.setText(user.getUserName());
						password.setText(user.getPassword());
						address.setText(user.getAddress());
						contactNo.setText(user.getContactNo());
						designation.setText(user.getDesignation());
						
					}
				};
				return cell;
			}
		};

		
		
		/*
		 *  Add All users to observable list and update table
		 */
		private void loadUserDetails(){
			
			userList.addAll(employeeService.getEmployeeList(getDesignation()));
			userTable.setItems(userList);
			
		}
		
		/*
		 * Validations
		 */
		private boolean validate(String field, String value, String pattern){
			if(!value.isEmpty()){
				Pattern p = Pattern.compile(pattern);
		        Matcher m = p.matcher(value);
		        if(m.find() && m.group().equals(value)){
		            return true;
		        }else{
		        	validationAlert(field, false);            
		            return false;            
		        }
			}else{
				validationAlert(field, true);            
	            return false;
			}        
	    }
		
		private boolean emptyValidation(String field, boolean empty){
	        if(!empty){
	            return true;
	        }else{
	        	validationAlert(field, true);            
	            return false;            
	        }
	    }	
		
		private void validationAlert(String field, boolean empty){
			Alert alert = new Alert(AlertType.WARNING);
	        alert.setTitle("Validation Error");
	        alert.setHeaderText(null);
	        if(field.equals("Role")) alert.setContentText("Please Select "+ field);
	        else{
	        	if(empty) alert.setContentText("Please Enter "+ field);
	        	else alert.setContentText("Please Enter Valid "+ field);
	        }
	        alert.showAndWait();
		}
	}