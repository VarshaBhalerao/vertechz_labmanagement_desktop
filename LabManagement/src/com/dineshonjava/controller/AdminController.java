package com.dineshonjava.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.stage.Stage;

public class AdminController implements Initializable{

	
	@FXML
	 private void Logout(ActionEvent event) throws IOException 
	{
		((Node) event.getSource()).getScene().getWindow().hide();
		Stage stage =new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Main_Login.fxml"));
		
		Scene scene = new Scene(root);
		//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		stage.setScene(scene);
	    stage.show(); 
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	
	//	userTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		//setColumnProperties();
		
		// Add all users into table
		
	}
	
}
