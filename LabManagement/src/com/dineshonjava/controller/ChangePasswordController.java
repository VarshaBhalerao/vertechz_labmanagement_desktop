package com.dineshonjava.controller;

import java.io.IOException;
import java.util.List;

import com.dineshonjava.model.chief_login;
import com.dineshonjava.service.EmployeeService;
import com.dineshonjava.service.EmployeeServiceImpl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class ChangePasswordController
{
	private EmployeeService employeeService = new EmployeeServiceImpl();
	@FXML
	private TextField user_change_Admin;
	@FXML
	private TextField password_change_Admin;
	@FXML
	private TextField cnfpassword_change_Admin;
	@FXML
	private TextField newpassword_change_Admin;
	
	@FXML
	public String getNewpassword_Change_Admin()
	{
		return newpassword_change_Admin.getText();
	}
		
	
	
	@FXML
	public String getUser_change_Admin() {
		return user_change_Admin.getText();
	}
	public String getPassword_change_Admin() {
		return password_change_Admin.getText();
	}
	public String getCnfpassword_change_Admin() {
		return cnfpassword_change_Admin.getText();
	}
	
	public void SUBMIT_CHANGE_ADMIN(ActionEvent event) throws IOException
	{
		System.out.println("In Change password ");
		
		System.out.println(getUser_change_Admin());
		System.out.println(getPassword_change_Admin());
			System.out.println(getNewpassword_Change_Admin());
			System.out.println(getCnfpassword_change_Admin());
		
			//String new_password=getCnfpassword_change_Admin();
			if(getNewpassword_Change_Admin().equals(getCnfpassword_change_Admin()))
			{
				
				
				List<chief_login> list = employeeService.listChief();
				int i =0;
				for(chief_login chief:list)
				{
					System.out.println("in for...." + ++i);
					if(getUser_change_Admin().equals(chief.getUserName()))
					{
						if(getPassword_change_Admin().equals(chief.getPassword()))
						{
							chief.setPassword(getNewpassword_Change_Admin());
							//chief.getUserName();
							//chief.getId();
						   employeeService.addchief(chief);
						    System.out.println(" password change..");
						    Alert alert = new Alert(AlertType.WARNING);
					        alert.setTitle("Validation Error");
					        alert.setHeaderText(null);
					        alert.setContentText("Password Change Sucessfully");
					        ((Node) event.getSource()).getScene().getWindow().hide();
			    			Stage stage =new Stage();
			    			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/ADMIN_HOME.fxml"));
			    			
			    			Scene scene = new Scene(root);
			    			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			    			stage.setScene(scene);
			    		    stage.show();
						    
						}
					}
				}
			}

		
	}
	@FXML
	public void RESET_CHANGE_ADMIN(ActionEvent event)
	{
		
	}
}
