package com.dineshonjava.controller;

public class User_LogIn
{
	public static String userName;
	public static int rid;

	public static int getRid() {
		return rid;
	}

	public static void setRid(int rid) {
		User_LogIn.rid = rid;
	}

	public static String getUserName() {
		return userName;
	}

	public static void setUserName(String userName) {
		User_LogIn.userName = userName;
	}

}
