package com.dineshonjava.controller;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailAuthenticationException;

import com.dineshonjava.model.chief_login;
import com.dineshonjava.service.EmployeeService;
import com.dineshonjava.service.EmployeeServiceImpl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class ForgotPasswordController
{
	private EmployeeService employeeService = new EmployeeServiceImpl();
	@FXML
	private TextField emailId_Admin;
	@FXML
	private TextField userName_Admin;
	private  String newpassword;
	
	public String getEmailId_Admin() {
		return emailId_Admin.getText();
	}
	public String getUserName_Admin() {
		return userName_Admin.getText();
	}
	
	@FXML
	private void Submit(ActionEvent event) 
	{
	 try
	 {
		 
	
		System.out.println("Inside forgot controller");
		
		String recipientAddress = getEmailId_Admin();
		String username=getUserName_Admin();
		
		List<chief_login> list = employeeService.listChief();
		int i =0;
		for(chief_login chief:list)
		 { 
			System.out.println("befor for");
			System.out.println("in for...." + ++i);
			///System.out.println(cpwd.getUserName());
			if(getUserName_Admin().equals(chief.getUserName()))
			 {
	           String uname=chief.getUserName();
	           System.out.println("uname From database="+uname);
				String subject ="Reset password";
				StringBuilder sb = new StringBuilder(username);
				Random random = new Random();
				int random_no=random.nextInt();
				
				String password = "S"+random_no;
				newpassword=username.concat(password);
				String message ="Dear User,Your password is  reset and your new password is "+newpassword+". Thank you.";
				
				// prints debug info
				System.out.println("To: " + recipientAddress);
				System.out.println("Subject: " + subject);
				System.out.println("Message: " + message);
				 chief.setPassword(newpassword);
			      employeeService.addchief(chief);

				// creates a simple e-mail object
				EmailInfo info=new EmailInfo();
	    		Session session = Session.getInstance(info.mailproperties(),
	    		  new javax.mail.Authenticator() 
	    			{
	    			protected PasswordAuthentication getPasswordAuthentication()
	    			{
	    				return new PasswordAuthentication(info.getmailFrom_username(), info.getmailFrom_password());
	    			}
	    		  });
    			Message message_1 = new MimeMessage(session);
    			message_1.setFrom(new InternetAddress("username"));
    			message_1.setRecipients(Message.RecipientType.TO,
    				InternetAddress.parse(getEmailId_Admin()));
    			message_1.setSubject("UserName and Password Information");
    			message_1.setText(message);

    			Transport.send(message_1);

    			System.out.println("Done");
    			((Node) event.getSource()).getScene().getWindow().hide();
    			Stage stage =new Stage();
    			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Main_Login.fxml"));
    			
    			Scene scene = new Scene(root);
    			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
    			stage.setScene(scene);
    		    stage.show();
			 }
		 }
	  }
			catch(AddressException ex)
			{
				try
				{
				Alert alert = new Alert(AlertType.WARNING);
		        alert.setTitle("Validation Error");
		        alert.setHeaderText(null);
		        alert.setContentText("Due to some problem mail not send .Your new Password is"+newpassword);
		        ((Node) event.getSource()).getScene().getWindow().hide();
    			Stage stage =new Stage();
    			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Main_Login.fxml"));
    			
    			Scene scene = new Scene(root);
    			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
    			stage.setScene(scene);
    		    stage.show();
				}
				catch(IOException ex4)
				
				{
					ex4.printStackTrace();
				}
			 }
			catch(MessagingException ex1)
			{
				try
				{
				Alert alert = new Alert(AlertType.WARNING);
		        alert.setTitle("Validation Error");
		        alert.setHeaderText(null);
		        alert.setContentText("Due to some problem mail not send .Your new Password is"+newpassword);
		        ((Node) event.getSource()).getScene().getWindow().hide();
    			Stage stage =new Stage();
    			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Main_Login.fxml"));
    			
    			Scene scene = new Scene(root);
    			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
    			stage.setScene(scene);
    		    stage.show();
				}
				catch(IOException ex4)
				
				{
					ex4.printStackTrace();
				}
				
			}
			catch(IOException ex2)
			{
				ex2.printStackTrace();
			}
			
			}
		
		
			
		

		
		

	@FXML
	 private void Reset(ActionEvent event)
	{
		userName_Admin.clear();
		emailId_Admin.clear();

		
	}
	
}
