package com.dineshonjava.controller;

import java.net.URL;
import java.util.ResourceBundle;

import com.dineshonjava.model.Records;
import com.dineshonjava.service.EmployeeService;
import com.dineshonjava.service.EmployeeServiceImpl;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class Selflife_ReminderController implements Initializable {
	@FXML
	public DatePicker reminder_date1;
	@FXML
	private Label recordId;
	
	 private EmployeeService employeeService = new EmployeeServiceImpl();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ShowReminderDate();
		
	}
	

    private void ShowReminderDate() {
		
    	int rid=User_LogIn.getRid();
		System.out.println("In ShowReminderDate ="+rid);
		 Records records = employeeService.getRecords(rid);
		 System.out.println("records.getReminder_date() : "+records.getReminder_date());
		 System.out.println("records.getRid() : "+records.getRid());
		 recordId.setText(Long.toString(records.getRid()));
		 reminder_date1.setValue(records.getReminder_date());
		 
	}
    
    @FXML
	public void saveUser(ActionEvent event) throws Exception{
    	int rid=User_LogIn.getRid();
		System.out.println("UserName After Login..="+rid);
		
		    Records records = employeeService.getRecords(rid);
		    records.setDate(records.getDate());
	 	    records.setFormulation(records.getFormulation());
	 	    records.setApproved_by(records.getApproved_by());
	 	    records.setResearch_executive(records.getResearch_executive());
	 	    records.setStatus(records.getStatus());
	 	    records.setObservations(records.getObservations());
	 	    records.setReminder_date(reminder_date1.getValue());
	 	    records.setProcess_details(records.getProcess_details());
	 	    records.setObjective(records.getObjective());
		 employeeService.addRecords(records);
		 saveAlert(records);
    }
	
    @FXML
     private void saveAlert(Records records){
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("User saved successfully.");
		alert.setHeaderText(null);
		alert.setContentText("The Record With Referance  Id "+records.getRid()+" "+" has been Saved .");
		alert.showAndWait();
	}
     
     @FXML
 	public void CloseAndReturn(ActionEvent event)throws Exception
 	{
 		
 		
 		Stage primaryStage=new Stage();
 		primaryStage.close();
 			
 		
 			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Chief_Record_Entry_Next.fxml"));
 			Scene scene = new Scene(root);
 			//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
 			primaryStage.setScene(scene);
 			primaryStage.show();
 	}
     
     @FXML
		private void exit(ActionEvent event) {
			Platform.exit();
	    }
	 @FXML
	    void reset(ActionEvent event) {
	    	clearFields();
	    }
    
	 @FXML
	private void clearFields() {
		reminder_date1.setAccessibleText(null);
		
	}
		
}
