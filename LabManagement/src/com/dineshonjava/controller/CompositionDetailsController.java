package com.dineshonjava.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.dineshonjava.model.Composition;
import com.dineshonjava.service.EmployeeService;
import com.dineshonjava.service.EmployeeServiceImpl;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Callback;

public class CompositionDetailsController {
	
	
	private EmployeeService employeeService = new EmployeeServiceImpl();
	@FXML
    private Button btnLogout;
	
	@FXML
    private Label compositionId;
	
	@FXML
    private TextField Constituent;
	
	@FXML
    private TextField Amount;
	
	@FXML
    private TextField Weight;
	
	
	
	@FXML
	private TableView<Composition> userTable;

	@FXML
	private TableColumn<Composition, Integer> colCompositionId;

	@FXML
	private TableColumn<Composition, String> colConstituent;
	
	@FXML
	private TableColumn<Composition, String> colAmount;
	@FXML
	private TableColumn<Composition, String> colWeight;

	@FXML
	private TableColumn<Composition, String> colStatus;

	@FXML
    private TableColumn<Composition, Boolean> colEdit;
	
	@FXML
    private MenuItem deleteUsers;
	
	/*@Lazy
    @Autowired
    private StageManager stageManager;
	
	@Autowired
	private UserService userService;
	*/
	private ObservableList<Composition> userList = FXCollections.observableArrayList();
	private ObservableList<String> roles = FXCollections.observableArrayList("Enabled", "Disabled");
	
	@FXML
	private void exit(ActionEvent event) {
		Platform.exit();
    }

	/**
	 * Logout and go to the login page
	 */
    @FXML
    private void logout(ActionEvent event) throws IOException {
    	//stageManager.switchScene(FxmlView.LOGIN);    	
    }
    
    @FXML
    void reset(ActionEvent event) {
    	clearFields();
    }
    
	
	
	@FXML
    private void saveUser(ActionEvent event){
    	
    	if(validate("Constituent", getConstituent(), "[a-zA-Z]+") && 
    	validate("Amount", getAmount(), "[0-9]+") &&	
    	validate("Weight", getWeight(), "[0-9]+") ){
    		
    		if(compositionId.getText() == null || compositionId.getText() == ""){
    			
    				/*User user = new User();
        			user.setformulationType(getformulationType());
        			user.setLastName(getLastName());
        			user.setDob(getDob());
        			user.setGender(getGender());
        			user.setRole(getRole());
        			user.setEmail(getEmail());
        			user.setPassword(getPassword());
        			
        			User newUser = userService.save(user);*/
    			
    			Composition fbean = new Composition();
    			   // fbean.setAnalysisname(getAnalysisName());
    			    fbean.setConstituent(getConstituent());
    			    fbean.setAmount(getAmount());
    			    fbean.setPercentage(getWeight());
    			  //  fbean.setFormulationtype(getFormulationType());
    			   // fbean.setStatus(getStatus());
    			    employeeService.addComposition(fbean);
        			
        			saveAlert(fbean);
    			
    			
    		}else{
    			Composition fbean = employeeService.getComposition(Integer.parseInt(compositionId.getText()));
    			//fbean.setAnalysisname(getAnalysisName());
    			//fbean.setStatus(getStatus());
    			fbean.setConstituent(getConstituent());
    			fbean.setAmount(getAmount());
    			fbean.setPercentage(getWeight());
    			 employeeService.addComposition(fbean);
    			
    			updateAlert(fbean);
    		}
    		
    		clearFields();
    		loadUserDetails();
    	}
    	
    	
    }
    
    @FXML
    private void deleteUsers(ActionEvent event){
    	List<Composition> analysisList= userTable.getSelectionModel().getSelectedItems();
    	
    	Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText(null);
		alert.setContentText("Are you sure you want to delete selected?");
		Optional<ButtonType> action = alert.showAndWait();
		
	if(action.get() == ButtonType.OK)
	{
		for(Composition analysisBean:analysisList)
		{
			employeeService.deleteComposition(analysisBean);
		}
	}
    	
    	loadUserDetails();
    }
    
   	private void clearFields() {
		compositionId.setText(null);
		Constituent.clear();
		Amount.clear();
		Weight.clear();
		//status.getSelectionModel().clearSelection();
		
	}
	
	private void saveAlert(Composition fbean){
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("User saved successfully.");
		alert.setHeaderText(null);
		alert.setContentText("The user "+fbean.getConstituent()+" "+" has been created and  id is "+ fbean.getComposition_id() +".");
		alert.showAndWait();
	}
	
	private void updateAlert(Composition fbean){
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("User updated successfully.");
		alert.setHeaderText(null);
		alert.setContentText("The user "+fbean.getConstituent()+" has been updated.");
		alert.showAndWait();
	}
	
	

	public String getConstituent() {
		return Constituent.getText();
	}
	
	public String getAmount() {
		return Amount.getText();
	}
	
	public String getWeight() {
		return Weight.getText();
	}

	
	
	

	
  

	public void initialize() {
		
		//status.setItems(roles);
		
		userTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		setColumnProperties();
		
		// Add all users into table
		loadUserDetails();
	}
	
	
	
	/*
	 *  Set All userTable column properties
	 */
	private void setColumnProperties(){
		/* Override date format in table
		 * colDOB.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<LocalDate>() {
			 String pattern = "dd/MM/yyyy";
			 DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
		     @Override 
		     public String toString(LocalDate date) {
		         if (date != null) {
		             return dateFormatter.format(date);
		         } else {
		             return "";
		         }
		     }

		     @Override 
		     public LocalDate fromString(String string) {
		         if (string != null && !string.isEmpty()) {
		             return LocalDate.parse(string, dateFormatter);
		         } else {
		             return null;
		         }
		     }
		 }));*/
		
		/*colAnalysisId.setCellValueFactory(new PropertyValueFactory<analysis ,Integer>("id"));*/
		colConstituent.setCellValueFactory(new PropertyValueFactory<Composition,String>("constituent"));
		colAmount.setCellValueFactory(new PropertyValueFactory<Composition,String>("amount"));
		colWeight.setCellValueFactory(new PropertyValueFactory<Composition,String>("percentage"));
		//colStatus.setCellValueFactory(new PropertyValueFactory<analysis,String>("status"));
		colEdit.setCellFactory(cellFactory);
	}
	
	Callback<TableColumn<Composition, Boolean>, TableCell<Composition, Boolean>> cellFactory = 
			new Callback<TableColumn<Composition, Boolean>, TableCell<Composition, Boolean>>()
	{
		@Override
		public TableCell<Composition, Boolean> call( final TableColumn<Composition, Boolean> param)
		{
			final TableCell<Composition, Boolean> cell = new TableCell<Composition, Boolean>()
			{
				
				Image imgEdit = new Image(getClass().getResourceAsStream("/images/edit.png"));
				final Button btnEdit = new Button();
				
				@Override
				public void updateItem(Boolean check, boolean empty)
				{
					super.updateItem(check, empty);
					if(empty)
					{
						setGraphic(null);
						setText(null);
					}
					else{
						btnEdit.setOnAction(e ->{
							Composition fbean = getTableView().getItems().get(getIndex());
							updateUser(fbean);
						});
						
						btnEdit.setStyle("-fx-background-color: transparent;");
						ImageView iv = new ImageView();
				        iv.setImage(imgEdit);
				        iv.setPreserveRatio(true);
				        iv.setSmooth(true);
				        iv.setCache(true);
						btnEdit.setGraphic(iv);
						
						setGraphic(btnEdit);
						setAlignment(Pos.CENTER);
						setText(null);
					}
				}

				private void updateUser(Composition fbean) {
					compositionId.setText(Long.toString(fbean.getComposition_id()));
					Constituent.setText(fbean.getConstituent());
					Amount.setText(fbean.getAmount());
					Weight.setText(fbean.getPercentage());
					
					//status.getSelectionModel().select(fbean.getStatus());
				}
			};
			return cell;
		}
	};

	
	
	/*
	 *  Add All users to observable list and update table
	 */
	private void loadUserDetails(){
		userList.clear();
		userList.addAll(employeeService.listComposition());

		userTable.setItems(userList);
	}
	
	/*
	 * Validations
	 */
	private boolean validate(String field, String value, String pattern){
		if(!value.isEmpty()){
			Pattern p = Pattern.compile(pattern);
	        Matcher m = p.matcher(value);
	        if(m.find() && m.group().equals(value)){
	            return true;
	        }else{
	        	validationAlert(field, false);            
	            return false;            
	        }
		}else{
			validationAlert(field, true);            
            return false;
		}        
    }
	
	private boolean emptyValidation(String field, boolean empty){
        if(!empty){
            return true;
        }else{
        	validationAlert(field, true);            
            return false;            
        }
    }	
	
	private void validationAlert(String field, boolean empty){
		Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Validation Error");
        alert.setHeaderText(null);
        if(field.equals("Role")) alert.setContentText("Please Select "+ field);
        else{
        	if(empty) alert.setContentText("Please Enter "+ field);
        	else alert.setContentText("Please Enter Valid "+ field);
        }
        alert.showAndWait();
	}

	@FXML
	public void CloseAndReturn(ActionEvent event)throws Exception
	{
		
		
		Stage primaryStage=new Stage();
		primaryStage.close();
			
		
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Chief_Record_Entry_Next.fxml"));
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
	}

}
