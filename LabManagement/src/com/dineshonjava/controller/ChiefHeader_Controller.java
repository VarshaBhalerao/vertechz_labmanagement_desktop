package com.dineshonjava.controller;


	
	import java.io.IOException;
	import java.net.URL;
	import java.util.ResourceBundle;

import org.apache.commons.io.output.ThresholdingOutputStream;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
	import javafx.fxml.FXMLLoader;
	import javafx.fxml.Initializable;
	import javafx.scene.Node;
	import javafx.scene.Parent;
	import javafx.scene.Scene;
	import javafx.scene.control.Label;

import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

	public class ChiefHeader_Controller  implements Initializable 
	{

       
    
		@FXML
		private Parent root ;
		 @FXML
		    private Label UserName_Label;
		 private void loadUserDetails()
			{
				
				String UserName=User_LogIn.getUserName();
				System.out.println("UserName At Chief Home..="+UserName);
				UserName_Label.setText("WELCOME "+UserName);
				
				
			}
			@Override
			public void initialize(URL location, ResourceBundle resources)
			{
			
				loadUserDetails();
			}
			
		
			/*formulationtype.setOnAction(new EventHandler<ActionEvent>() {
				 
	            @Override
	            public void handle(ActionEvent event) {
	               
	            }
	        });*/
			@FXML
			MenuItem formulationtype;
			
			public void formulationtype(ActionEvent event)throws Exception 
			{
             this.formulationtype.setOnAction(new EventHandler<ActionEvent>() {
            	    @Override public void handle(ActionEvent e) {
            	    	
            	    	
        				Stage stage =new Stage();
        				Parent root1;
						try {
							root1 = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Formulation.fxml"));
							Scene scene = new Scene(root1);
	        				//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
	        				stage.setScene(scene);
	        			    stage.show();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
        				
        				
            	    }
            	});
				
			}
			@FXML
			
			public void analysistype(ActionEvent event)throws Exception 
			{
				//((Node) event.getSource()).getScene().getWindow().hide();
				Stage stage =new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Analysis.fxml"));
				
				Scene scene = new Scene(root);
				//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				stage.setScene(scene);
			    stage.show();
			}
			@FXML
			public void atsdays(ActionEvent event)throws Exception 
			{
				//((Node) event.getSource()).getScene().getWindow().hide();
				Stage stage =new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Ats.fxml"));
				
				Scene scene = new Scene(root);
				//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				stage.setScene(scene);
			    stage.show();
			}
			@FXML
			public void coldtestdays(ActionEvent event)throws Exception 
			{
				//((Node) event.getSource()).getScene().getWindow().hide();
				Stage stage =new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/ColdTestDays.fxml"));
				
				Scene scene = new Scene(root);
				//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				stage.setScene(scene);
			    stage.show();
			}
			@FXML
			public void labrecordentry(ActionEvent event)throws Exception 
			{
				//((Node) event.getSource()).getScene().getWindow().hide();
				Stage stage =new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/ColdTestDays.fxml"));
				
				Scene scene = new Scene(root);
				//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				stage.setScene(scene);
			    stage.show();
			}
			@FXML
			public void labrecordentry_viewall(ActionEvent event)throws Exception 
			{
				//((Node) event.getSource()).getScene().getWindow().hide();
				Stage stage =new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/ColdTestDays.fxml"));
				
				Scene scene = new Scene(root);
				//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				stage.setScene(scene);
			    stage.show();
			}
			@FXML
			public void labrecordsearch_all(ActionEvent event)throws Exception 
			{
				//((Node) event.getSource()).getScene().getWindow().hide();
				Stage stage =new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/ChangePassword.fxml"));
				
				Scene scene = new Scene(root);
				//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				stage.setScene(scene);
			    stage.show();
			}
			@FXML
			public void labrecordsearch(ActionEvent event)throws Exception 
			{
				//((Node) event.getSource()).getScene().getWindow().hide();
				Stage stage =new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/ChangePassword.fxml"));
				
				Scene scene = new Scene(root);
				//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				stage.setScene(scene);
			    stage.show();
			}
			@FXML
			public void admin_changepassword(ActionEvent event)throws Exception 
			{
				//((Node) event.getSource()).getScene().getWindow().hide();
				Stage stage =new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/ChangePassword.fxml"));
				
				Scene scene = new Scene(root);
				//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				stage.setScene(scene);
			    stage.show();
			}
			@FXML
			 private void logout(ActionEvent event) throws IOException 
			{
				((Node) event.getSource()).getScene().getWindow().hide();
				Stage stage =new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Main_Login.fxml"));
				
				Scene scene = new Scene(root);
				//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				stage.setScene(scene);
			    stage.show(); 
			}
	}