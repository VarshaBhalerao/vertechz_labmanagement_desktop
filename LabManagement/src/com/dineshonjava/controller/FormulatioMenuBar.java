package com.dineshonjava.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FormulatioMenuBar implements Initializable {

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
	@FXML
	public void Formulation(ActionEvent event)throws Exception
	{
		
		
		Stage primaryStage=new Stage();
		primaryStage.close();
			
		
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Formulation.fxml"));
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
	}
	
	@FXML
	public void AnalysisTypes(ActionEvent event)throws Exception
	{
		
		
		Stage primaryStage=new Stage();
		primaryStage.close();
			
		
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Analysis.fxml"));
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
	}
	
	@FXML
	public void Ats(ActionEvent event)throws Exception
	{
		
		
		Stage primaryStage=new Stage();
		primaryStage.close();
			
		
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Ats.fxml"));
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
	}
	@FXML
	public void ColdTestDays(ActionEvent event)throws Exception
	{
		
		
		Stage primaryStage=new Stage();
		primaryStage.close();
			
		
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/ColdTestDays.fxml"));
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
	}
	
	
	@FXML
	public void Chief_Record_Entry(ActionEvent event)throws Exception
	{
		
		
		Stage primaryStage=new Stage();
		primaryStage.close();
			
		
			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Chief_Record_Entry.fxml"));
			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
	}
	

}
