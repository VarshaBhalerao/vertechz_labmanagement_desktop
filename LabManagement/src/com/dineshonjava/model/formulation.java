package com.dineshonjava.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="formulation")
public class formulation  {

	//private static final long serialVersionUID = 1L;

	@Id @GeneratedValue
	private Integer id;
	
	@Column(name="formulation_type")
	private String formulationtype;
	
	@Column(name="status")
	private String status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFormulationtype() {
		return formulationtype;
	}
	public void setFormulationtype(String formulationtype) {
		this.formulationtype = formulationtype;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

	

}
