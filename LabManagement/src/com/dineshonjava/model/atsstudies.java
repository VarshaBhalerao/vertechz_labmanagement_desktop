package com.dineshonjava.model;
//
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="atsstudies")
public class atsstudies {
	
	@Id
	@GeneratedValue
	int ats_id;
	public int getAts_id() {
		return ats_id;
	}
	public void setAts_id(int ats_id) {
		this.ats_id = ats_id;
	}
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public String getAnalyticalstudies() {
		return analyticalstudies;
	}
	public void setAnalyticalstudies(String analyticalstudies) {
		this.analyticalstudies = analyticalstudies;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	int rid;
	String analyticalstudies;
	String days;
	String remarks;

	

}
