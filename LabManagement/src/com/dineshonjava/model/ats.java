package com.dineshonjava.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
//
@Entity(name="ats")
public class ats {
	@Id @GeneratedValue
	private int id;
	
	@Column(name="days_description")
	private String days_description;
	
	@Column(name="status")
	private String status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDays_description() {
		return days_description;
	}

	public void setDays_description(String days_description) {
		this.days_description = days_description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}	
	
}
