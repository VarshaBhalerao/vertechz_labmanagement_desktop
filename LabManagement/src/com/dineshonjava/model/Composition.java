package com.dineshonjava.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="composition")
public class Composition {
@Id
@GeneratedValue
int composition_id;
public int getComposition_id() {
	return composition_id;
}
public void setComposition_id(int composition_id) {
	this.composition_id = composition_id;
}
public int getRid() {
	return rid;
}
public void setRid(int rid) {
	this.rid = rid;
}
public String getConstituent() {
	return constituent;
}
public void setConstituent(String constituent) {
	this.constituent = constituent;
}
public String getAmount() {
	return amount;
}
public void setAmount(String amount) {
	this.amount = amount;
}
public String getPercentage() {
	return percentage;
}
public void setPercentage(String percentage) {
	this.percentage = percentage;
}
int rid;
String constituent;
String amount;
String percentage;

}
