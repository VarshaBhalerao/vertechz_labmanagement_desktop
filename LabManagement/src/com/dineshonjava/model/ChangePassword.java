package com.dineshonjava.model;



public class ChangePassword {
   
	private String userName;
	
	private String oldPassword;
	
	private String newPassword;
	
	private String cnfPassword;
	
	 public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getUserName() {
			return userName;
		}
	public void setOldPassword(String oldPassword)
	{
		this.oldPassword = oldPassword;
	}
	public void setNewPassword(String newPassword) 
	{
		this.newPassword = newPassword;
	}
	public void setCnfPassword(String cnfPassword)
{
		this.cnfPassword = cnfPassword;
	}
	public String getOldPassword()
	{
		return oldPassword;
	}
	public String getNewPassword()
	{
		return newPassword;
	}
	public String getCnfPassword()
	{
		return cnfPassword;
	}
	

}
