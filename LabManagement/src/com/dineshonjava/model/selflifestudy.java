package com.dineshonjava.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class selflifestudy {
	@Id
	@GeneratedValue
	int selflife_id;
	public int getSelflife_id() {
		return selflife_id;
	}
	public void setSelflife_id(int selflife_id) {
		this.selflife_id = selflife_id;
	}
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	
	public String getStudydate() {
		return studydate;
	}
	public void setStudydate(String studydate) {
		this.studydate = studydate;
	}

	int rid;
    String days;
    String observation;
    
    String studydate;

}
