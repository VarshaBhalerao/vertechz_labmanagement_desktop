package com.dineshonjava.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="ActiveInfo")
public class ActiveInfo {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
     int id;	
	@Column(name="active")
	String active;
	
	@Column(name="concentration")
	String concentration;
 

    @OneToMany(targetEntity=formulation.class,
                                 cascade=CascadeType.ALL,
                                 fetch=FetchType.EAGER,
                                 orphanRemoval=true)
    @JoinColumn(name="unid", referencedColumnName="id")
       private Set<formulation> formulationType;
	
	public Set<formulation> getFormulationType() {
		return formulationType;
	}

	public void setFormulationType(Set<formulation> formulationType) {
		this.formulationType = formulationType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getConcentration() {
		return concentration;
	}

	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}
	
  
}
