package com.dineshonjava.model;


import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity (name="records")
public class Records {
	
	@Id
	@GeneratedValue
	int rid;
	public String records_active;
	public String getRecords_active() {
		return records_active;
	}
	public void setRecords_active(String records_active) {
		this.records_active = records_active;
	}
	LocalDate date;
	String date1;
	String reminder_date1;
	public LocalDate getDate() {
		return date;
	}
	public String getDate1() {
		return date1;
	}
	public void setDate1(String date1) {
		this.date1 = date1;
	}
	public String getReminder_date1() {
		return reminder_date1;
	}
	public void setReminder_date1(String reminder_date1) {
		this.reminder_date1 = reminder_date1;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public LocalDate getReminder_date() {
		return reminder_date;
	}
	public void setReminder_date(LocalDate reminder_date) {
		this.reminder_date = reminder_date;
	}
	LocalDate reminder_date;
	String referance_no;
	String research_executive;
	String approved_by;
	String objective;
	String formulation;
	String process_details;
	String Observations;
	String status;
	
	
	
	
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}

	public String getReferance_no() {
		return referance_no;
	}
	public void setReferance_no(String referance_no) {
		this.referance_no = referance_no;
	}
	public String getResearch_executive() {
		return research_executive;
	}
	public void setResearch_executive(String research_executive) {
		this.research_executive = research_executive;
	}
	public String getApproved_by() {
		return approved_by;
	}
	public void setApproved_by(String approved_by) {
		this.approved_by = approved_by;
	}
	public String getObjective() {
		return objective;
	}
	public void setObjective(String objective) {
		this.objective = objective;
	}
	public String getFormulation() {
		return formulation;
	}
	public void setFormulation(String formulation) {
		this.formulation = formulation;
	}
	public String getProcess_details() {
		return process_details;
	}
	public void setProcess_details(String process_details) {
		this.process_details = process_details;
	}
	public String getObservations() {
		return Observations;
	}
	public void setObservations(String observations) {
		Observations = observations;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}


}
