package com.dineshonjava.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="chieflogin",uniqueConstraints={@UniqueConstraint(columnNames={"emailId","emailId"})})
public class chief_login implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	 @NotEmpty
	 @Column(name="fname",nullable=false)
	private String fname;
	 @NotEmpty
	@Column(name="lname",nullable=false)
	private String lname;
	@NotEmpty
	@Column(name="emailId",nullable=false)
	private String emailId;
	@Column(name="username",nullable=false)
	private String userName;
	
	@Column(name="password",nullable=false)
	private String password;
	
	 @NotEmpty
	@Column(name="address",nullable=false)
	private String address;
	 @NotEmpty
	@Column(name="contactNo",nullable=false)
	private String contactNo;
	 @NotEmpty
	@Column(name="designation",nullable=false)
	private String designation;
	 
	public int getId() {
		return id;
	}

	public String getFname() {
		return fname;
	}

	public String getLname() {
		return lname;
	}

	public String getEmailId() {
		return emailId;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public String getAddress() {
		return address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public String getDesignation() {
		return designation;
	}

	public String getCountry() {
		return country;
	}

	public Date getRegDate() {
		return RegDate;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setRegDate(Date regDate) {
		RegDate = regDate;
	}

	@Column(name="country")
	private String country;

	@Column(name="RegDate", nullable=false)
	private Date RegDate;

	
}
