package com.dineshonjava.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity (name="active")
public class Active {
	@Id
	@GeneratedValue
	int active_id;
	int rid;
	String active;
	String concentration;
	
	
	public int getActive_id() {
		return active_id;
	}
	
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public void setActive_id(int active_id) {
		this.active_id = active_id;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getConcentration() {
		return concentration;
	}
	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}
	

}
