package com.dineshonjava.model;
/*==*/
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name="analyticalstudy1")
public class analyticalstudy1 {
	
	@Id
	 @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="analyticalstudy1_id") 
	int analyticalstudy1_id;
	int analyticalstudy_id;
	public int getAnalyticalstudy_id() {
		return analyticalstudy_id;
	}
	public void setAnalyticalstudy_id(int analyticalstudy_id) {
		this.analyticalstudy_id = analyticalstudy_id;
	}
    public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	int rid; 
	   
	
	public int getAnalyticalstudy1_id() {
		return analyticalstudy1_id;
	}
	public void setAnalyticalstudy1_id(int analyticalstudy1_id) {
		this.analyticalstudy1_id = analyticalstudy1_id;
	}

	String analytical_active;

	public String getAnalytical_active() {
		return analytical_active;
	}
	public void setAnalytical_active(String analytical_active) {
		this.analytical_active = analytical_active;
	}
	public String getConcentration() {
		return concentration;
	}
	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}
	String concentration;

}
