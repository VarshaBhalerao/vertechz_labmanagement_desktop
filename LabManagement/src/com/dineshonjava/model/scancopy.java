package com.dineshonjava.model;



import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;




@Entity (name="scancopy")
public class scancopy {
	
	@Id
	@GeneratedValue
	private int scan_id;	
	
	private int rid;	
	@Lob
	private Blob scanfile;
	
	@Column(name="name")
	private String name;
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}		
	public Blob getScanfile() {
		return scanfile;
	}
	public void setScanfile(Blob scanfile) {
		this.scanfile = scanfile;
	}
	public int getScan_id() {
		return scan_id;
	}
	public void setScan_id(int scan_id) {
		this.scan_id = scan_id;
	}
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	
	
	

}
