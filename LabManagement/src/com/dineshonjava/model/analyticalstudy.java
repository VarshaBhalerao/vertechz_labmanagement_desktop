package com.dineshonjava.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity 
@Table (name="analyticalstudy")
public class analyticalstudy {
	
	@Id
	 @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="analyticalstudy_id")

	int analyticalstudy_id;
	int rid;
	
	/* @OneToMany
	    @JoinTable( name="analyticalstudy_joint",
	                joinColumns= @JoinColumn(name="analyticalstudy_id"),
	                inverseJoinColumns= @JoinColumn(name="analyticalstudy1_id"))
	 private Collection<analyticalstudy1> analyticalstudy = new ArrayList<analyticalstudy1>();
	public Collection<analyticalstudy1> getAnalyticalstudy() {
		return analyticalstudy;
	}
	public void setAnalyticalstudy(Collection<analyticalstudy1> analyticalstudy) {
		this.analyticalstudy = analyticalstudy;
	}*/
	    
	/*String active;
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getConcentration() {
		return concentration;
	}
	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}
	String concentration;*/
	String analytical_study;

	
	public int getAnalyticalstudy_id() {
		return analyticalstudy_id;
	}
	public void setAnalyticalstudy_id(int analyticalstudy_id) {
		this.analyticalstudy_id = analyticalstudy_id;
	}
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public String getAnalytical_study() {
		return analytical_study;
	}
	public void setAnalytical_study(String analytical_study) {
		this.analytical_study = analytical_study;
	}
	

}
