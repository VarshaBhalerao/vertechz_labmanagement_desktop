package com.dineshonjava.service;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;



import com.dineshonjava.dao.EmployeeDao;
import com.dineshonjava.dao.EmployeeDaoImpl;
import com.dineshonjava.model.Active;
import com.dineshonjava.model.Composition;

import com.dineshonjava.model.formulation;
import com.dineshonjava.model.image;
import com.dineshonjava.model.recordcomment;
import com.dineshonjava.model.scancopy;
import com.dineshonjava.model.selflifestudy;
import com.dineshonjava.model.Records;
import com.dineshonjava.model.chief_login;
import com.dineshonjava.model.coldtest;
import com.dineshonjava.model.coldtestdays;
import com.dineshonjava.model.analysis;
import com.dineshonjava.model.analyticalstudy;
import com.dineshonjava.model.ats;
import com.dineshonjava.model.atsstudies;

/**
 * @author Dinesh Rajput

 */
@Service("employeeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class EmployeeServiceImpl implements EmployeeService {

	
	private EmployeeDao employeeDao = new EmployeeDaoImpl();
	
	

	
	
	@Override
	public List<chief_login> listChief() {
		return employeeDao.listChief();
	}
	
	
		/* varsha */	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addFormulation(formulation formulation) {
		employeeDao.addFormulation(formulation);
	}
	@Override
	public List<formulation> listFormulation() {
		return employeeDao.listFormulation();
	}	
	@Override
	public formulation getFormulation(int id) {
		return employeeDao.getFormulation(id);
	}	
	@Override
	public void deleteFormulation(formulation formulationBean) {
		employeeDao.deleteFormulation(formulationBean);
		
	}
	@Override
	public List<formulation> getFormulationList(int result, int offset_real) {
		return employeeDao.getFormulationList(result, offset_real); 
	}
	@Override
	public int getFormulationSize() {
		return employeeDao.getFormulationSize();
	}
	
	
	
	
	
	

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAnalysis(analysis analysis) {
		employeeDao.addAnalysis(analysis);		
	}
	@Override
	public List<analysis> listAnalysis() {
		return employeeDao.listAnalysis();
	}
	@Override
	public analysis getAnalysis(int id) {
		return employeeDao.getAnalysis(id);
	}
	@Override
	public void deleteAnalysis(analysis analysisBean) {
		employeeDao.deleteAnalysis(analysisBean);		
	}
	@Override
	public List<analysis> getAnalysisList(int result, int offset_real) {
		return employeeDao.getAnalysisList(result, offset_real);
	}
	@Override
	public int getAnalysis_Size() {
		return employeeDao.getAnalysis_Size();
	}
	
	
	
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addATS(ats atsBean) {
		employeeDao.addATS(atsBean);	
	}
	@Override
	public List<ats> listATS() {
		return employeeDao.listATS();
	}
	@Override
	public ats getATS(int id) {
		return employeeDao.getATS(id);
	}
	@Override
	public void deleteATS(ats atsBean) {
		employeeDao.deleteATS(atsBean);
	}
	@Override
	public List<ats> getATSList(int result, int offset_real) {
		return employeeDao.getATSList(result, offset_real);
	}
	@Override
	public int getATS_Size() {
		return employeeDao.getATS_Size();
	}
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addColdTestDays(coldtestdays coldtestdaysBean) {
		employeeDao.addColdTestDays(coldtestdaysBean);		
	}
	@Override
	public List<coldtestdays> listColdTestDays() {
		return employeeDao.listColdTestDays();
	}
	@Override
	public coldtestdays getColdTestDays(int id) {
		return employeeDao.getColdTestDays(id);
	}
	@Override
	public void deleteColdTestDays(coldtestdays coldtestdaysBean) {
		employeeDao.deleteColdTestDays(coldtestdaysBean);
	}
	@Override
	public List<coldtestdays> getColdTestDaysList(int result, int offset_real) {
		return employeeDao.getColdTestDaysList(result, offset_real);
	}
	@Override
	public int getColdTestDays_Size() {
		return employeeDao.getColdTestDays_Size();
	}
	
	
	
	
	
	
	
	
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addImage(image imageBean) {
		employeeDao.addImage(imageBean);	
	}
	@Override
	public List<image> listImage() {
		return employeeDao.listImage();
	}
	@Override
	public List<image> listImage(int rid) {
		return employeeDao.listImage(rid);
	}
	@Override
	public image getImage(int id) {
		return employeeDao.getImage(id);
	}
	@Override
	public void deleteImage(image imageBean) {
		employeeDao.deleteImage(imageBean);
	}
	
	
	/* varsha */
	
	
	
	
	
	
	/* bhushan */
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addRecords(Records records) {
		employeeDao.addRecords(records);	
	}
	@Override
	public List<Records> listRecords() {
		return employeeDao.listRecords();
	}
	public Records getRecords(int rid) {
		return employeeDao.getRecords(rid);
	}
	@Override
	public void deleteRecord(Records records) {
		employeeDao.deleteRecord(records);
	}
	@Override
	public List<Records> getRecordsList(int result, int offset_real) {
		return employeeDao.getRecordsList(result, offset_real);
	}
	@Override
	public int getRecords_Size() {
		return employeeDao.getRecords_Size();
	}
	
	
	
	
	
	@Override
	public List<Active> listActive() {
		return employeeDao.listActive();
	}
	@Override
	public List<Active> listActive(int rid) {
		return employeeDao.listActive(rid);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addActive(Active active) {
		employeeDao.addActive(active);	
	}
	@Override
	public void deleteActive(Active active) {	
		employeeDao.deleteActive(active);
	}
	@Override
	public Active getActive(int active_id) {
		return employeeDao.getActive(active_id);
	}

	

	@Override
	public List<Composition> listComposition() {
		return employeeDao.listComposition();
	}
	@Override
	public List<Composition> listComposition(int rid) {
		return employeeDao.listComposition(rid);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addComposition(Composition composition) {
		employeeDao.addComposition(composition);
	}
	@Override
	public void deleteComposition(Composition composition) {
		employeeDao.deleteComposition(composition);
		
	}
	@Override
	public Composition getComposition(int conposition_id) {
		return employeeDao.getComposition(conposition_id);
	}

	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addSelfLife_Studies(selflifestudy selflifestudy) {
	   employeeDao.addSelfLife_Studies(selflifestudy);
		
	}


	@Override
	public List<selflifestudy> listSelfLife_Studies() {
		return employeeDao.listSelfLife_Studies();
	}


	@Override
	public List<selflifestudy> listSelfLife_Studies(int rid) {
		return employeeDao.listSelfLife_Studies(rid);
	}


	@Override
	public void deleteSelfLife_Studies(selflifestudy selflifestudy) {
	      employeeDao.deleteSelfLife_Studies(selflifestudy);
		
	}


	@Override
	public selflifestudy getSelfLife_Studies(int selflife_id) {
		return employeeDao.getSelfLife_Studies(selflife_id);
	}
	
	
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addAnalytical_Studies(analyticalstudy analyticalstudy) {
		employeeDao.addAnalytical_Studies( analyticalstudy);
	}
	
	@Override
	public void addAnalytical_Studies1(int analyticalstudy_id, String active, String concentration, int rid) {
		employeeDao.addAnalytical_Studies1(analyticalstudy_id, active, concentration, rid);
		
	}
	
	@Override
	public void updateAnalytical_Studies1(int analyticalstudy_id, String active, String concentration, int rid) {
		employeeDao.updateAnalytical_Studies1(analyticalstudy_id, active, concentration, rid);
		
	}
	
	@Override
	public void deleteAnalytical_Studies1(int analyticalstudy_id) {
		employeeDao.deleteAnalytical_Studies1(analyticalstudy_id);
		
	}
	
	@Override
	public void deleteAnalytical_Studies1(String active, int rid) {
		employeeDao.deleteAnalytical_Studies1(active, rid);
		
	}
	
	@Override
	public List<analyticalstudy> listAnalytical_Studies() {
		return employeeDao.listAnalytical_Studies();
	}
	@Override
	public List<analyticalstudy> listAnalytical_Studies(int rid) {
		return employeeDao.listAnalytical_Studies(rid);
	}
	@Override
	public void deleteAnalytical_Studies(analyticalstudy analyticalstudy) {
		employeeDao.deleteAnalytical_Studies( analyticalstudy);
	}
	@Override
	public analyticalstudy getAnalytical_Studies(int analyticalstudy_id) {
		return employeeDao.getAnalytical_Studies( analyticalstudy_id);
	}

	
	@Override
	public List<scancopy> listScanned_Copies() {
		return employeeDao.listScanned_Copies();
	}
	@Override
	public List<scancopy> listScanned_Copies(int rid) {
		return employeeDao.listScanned_Copies(rid);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addScanned_Copies(scancopy scancopy) {
		employeeDao.addScanned_Copies( scancopy);
		
	}
	@Override
	public void deleteScanned_Copies(scancopy scancopy) {
		employeeDao.deleteScanned_Copies(scancopy);
		
	}
	@Override
	public scancopy getScanned_Copies(int scan_id) {
		return employeeDao.getScanned_Copies(scan_id);

	}
	
	
	
	
	
	@Override
	public List<coldtest> listCold_Test() {
		return employeeDao.listCold_Test();
	}
	@Override
	public List<coldtest> listCold_Test(int rid) {
		return employeeDao.listCold_Test(rid);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addCold_Test(coldtest coldtest) {
		employeeDao.addCold_Test(coldtest);
		
	}
	@Override
	public void deleteCold_Test(coldtest coldtest) {
		employeeDao.deleteCold_Test(coldtest);
		
	}
	@Override
	public coldtest getCold_Test(int cold_id) {
		return employeeDao.getCold_Test(cold_id);
	}
	
	
	
	@Override
	public List<atsstudies> listATS_Studies() {
		return employeeDao.listATS_Studies();
	}
	@Override
	public List<atsstudies> listATS_Studies(int rid) {
		return employeeDao.listATS_Studies(rid);
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addATS_Studies(atsstudies atsstudies) {
		employeeDao.addATS_Studies( atsstudies);
		
	}
	@Override
	public void deleteATS_Studies(atsstudies atsstudies) {
		employeeDao.deleteATS_Studies(atsstudies);
	}
	@Override
	public atsstudies getATS_Studies(int ats_id) {
		return employeeDao.getATS_Studies( ats_id);
	}
	
	
	

	//Record Comment Start
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addRecord_Comment(recordcomment recordcomment) {
       employeeDao.addRecord_Comment(recordcomment);
		
	}


	@Override
	public List<recordcomment> getRecord_Comment(int rid) {
		return employeeDao.getRecord_Comment(rid);
	}


	@Override
	public void DeleteRecord_Comment(recordcomment recordcomment) {
		employeeDao.DeleteRecord_Comment(recordcomment);
		
	}

	//Record Comment End
	
	
	
	//SEARCH RECORDS OF CHIEF ONLY
	
		@Override
		public List<Records> ChiefRecordSerchBy_Status_Active(String status, String active, String username) {
			return employeeDao.ChiefRecordSerchBy_Status_Active(status, active, username);
		}


		@Override
		public List<Records> ChiefRecordSerchBy_Fomulation_Active(String formulation, String active, String username) {
			return employeeDao.ChiefRecordSerchBy_Fomulation_Active(formulation, active, username);
		}


		@Override
		public List<Records> ChiefRecordSerchBy_Fomulation_Status(String formulation, String status, String username) {
			return employeeDao.ChiefRecordSerchBy_Fomulation_Status(formulation, status, username);
		}


		@Override
		public List<Records> ChiefRecordSerchBy_Fomulation_Status_Active(String formulation, String status, String active, String username) {
			return employeeDao.ChiefRecordSerchBy_Fomulation_Status_Active( formulation, status, active, username);
		}
		
		@Override
		public List<Records> ChiefRecordSerchByFormulation(String formulation, String username) {
			// TODO Auto-generated method stub
			return employeeDao.ChiefRecordSerchByFormulation(formulation, username);
		}


		@Override
		public List<Records> ChiefRecordSerchByActive(String active, String username) {
			// TODO Auto-generated method stub
			return employeeDao.ChiefRecordSerchByActive(active, username) ;
		}


		@Override
		public List<Records> ChiefRecordSerchByStatus(String status, String username) {
			// TODO Auto-generated method stub
			return employeeDao.ChiefRecordSerchByStatus(status, username);
		}

	    //fetch record between dates
		@Override
		public List<Records> ChiefRecordSerchByDate(Date todate, Date fromdate, String username) {
			return employeeDao.ChiefRecordSerchByDate( todate, fromdate, username);
		}
		
		
		//fetch records by single dates
		@Override
		public List<Records> ChiefRecordSerchBySingleDate(Date todate, Date fromdate, String username) {
			return employeeDao.ChiefRecordSerchBySingleDate(todate,fromdate, username);
		}	
		
		

		@Override
		public List<Records> listChiefRecord(String username) {
			return employeeDao.listChiefRecord(username);
			
		}

	//SEARCH RECORDS OF CHIEF
	
//SEARCH RECORDS OF ALL
	
	@Override
	public List<Records> SerchBy_Status_Active(String status, String active) {
		return employeeDao.SerchBy_Status_Active(status, active);
	}


	@Override
	public List<Records> SerchBy_Fomulation_Active(String formulation, String active) {
		return employeeDao.SerchBy_Fomulation_Active(formulation, active);
	}


	@Override
	public List<Records> SerchBy_Fomulation_Status(String formulation, String status) {
		return employeeDao.SerchBy_Fomulation_Status(formulation, status);
	}


	@Override
	public List<Records> SerchBy_Fomulation_Status_Active(String formulation, String status, String active) {
		return employeeDao.SerchBy_Fomulation_Status_Active( formulation, status, active);
	}
	
	@Override
	public List<Records> SerchByFormulation(String formulation) {
		// TODO Auto-generated method stub
		return employeeDao.SerchByFormulation(formulation);
	}


	@Override
	public List<Records> SerchByActive(String active) {
		// TODO Auto-generated method stub
		return employeeDao.SerchByActive(active) ;
	}


	@Override
	public List<Records> SerchByStatus(String status) {
		// TODO Auto-generated method stub
		return employeeDao.SerchByStatus(status);
	}

    //fetch record between dates
	/*@Override
	public List<Records> SerchByDate(Date todate, Date fromdate ,String username) {
		return employeeDao.SerchByDate( todate, fromdate, username);
	}
	*/
	
	//fetch records by single dates

//SEARCH RECORDS OF ALL
 //to get count for current date entry Referance Number Enetring Records
	@Override
	public List<Records> TodayRecordCount(Date format) throws HibernateException, ParseException {
		return employeeDao.TodayRecordCount(format);	
	}

 //  Preview
	@Override
	public List<Active> List_Chief_Preview_Active(int rid) {
		return employeeDao.List_Chief_Preview_Active(rid);
	}


	@Override
	public List<Composition>  List_Chief_Preview_Composition(int rid){
		return employeeDao.List_Chief_Preview_Composition(rid);
	}
	
	
	@Override
	public List<analyticalstudy> List_Chief_Preview_Analytical_Studies(int rid) {
		return employeeDao.List_Chief_Preview_Analytical_Studies(rid);
	}


	@Override
	public List<atsstudies> List_Chief_Preview_ATS_Studies(int rid) {
		return employeeDao.List_Chief_Preview_ATS_Studies(rid);
	}


	@Override
	public List<coldtest> List_Chief_Preview_Cold_Test(int rid) {
		return employeeDao.List_Chief_Preview_Cold_Test(rid);
	}

	
	
	
	
	
	
	@Override
	public void addATS_Studies1(int ats_id, String active, String concentration, int rid) {
		employeeDao.addATS_Studies1(ats_id, active, concentration, rid);
		
	}


	@Override
	public void deleteATS_Studies1(String activevalue, int rid) {
		employeeDao . deleteATS_Studies1(activevalue,rid) ;
		
	}


	@Override
	public void deleteATS_Studies1(int ats_id) {
		employeeDao.deleteATS_Studies1(ats_id);
		
	}


	@Override
	public void updateATS_Studies1(int ats_id, String active, String concentration, int rid) {
		employeeDao.updateATS_Studies1(ats_id, active, concentration, rid);
		
	}


 //  Preview
	
	
	
	
	
	
	
	
	
	
	
	/* bhushan */

	
	/* Dipali*/

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addManager(chief_login manager) {
		employeeDao.addManager(manager);	
	}
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addchief(chief_login bean) {
		employeeDao.addchief(bean);
	}
	
	
	

	@Override
	public List<chief_login> getEmployeeList(String Designation) {
		// TODO Auto-generated method stub
		return employeeDao.getEmployeeList(Designation);
	}


	@Override
	public chief_login getEmp(int emp_id) {
		// TODO Auto-generated method stub
		return employeeDao.getEmp(emp_id);
	}


	@Override
	public void deleteEmp(chief_login managerBean) {
		// TODO Auto-generated method stub
		employeeDao.deleteEmp(managerBean);
	}

	
	
	

	@Override
	public List<Records> listRemainder1(String username) {
		// TODO Auto-generated method stub
		return employeeDao.listRemainder1(username);
	}	
	
	
	/* Dipali*/
	
	
	@Override
	public List<Records> listRemainder() {
		return employeeDao.listRemainder();
	}


	@Override
	public List<chief_login> ListChiefLogin() {
		return employeeDao.ListChiefLogin();
	}


	@Override
	public List<Records> ListRecords() {
		return employeeDao.ListRecords();
	}


	@Override
	public List<Records> SerchByDate(LocalDate parsedDate, LocalDate parsedDate2) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Records> SerchBySingleDate(LocalDate parsedDate, LocalDate parsedDate2) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Records> SerchByDate1(LocalDate parsedDate, LocalDate parsedDate2) {
		// TODO Auto-generated method stub
		return employeeDao.SerchByDate1(parsedDate, parsedDate2);
	}


	

	

	
	


	
	



	


	


	


	


	









	
	

}
