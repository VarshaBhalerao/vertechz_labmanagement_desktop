package com.dineshonjava.service;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;


import com.dineshonjava.model.Active;
import com.dineshonjava.model.Composition;

import com.dineshonjava.model.formulation;
import com.dineshonjava.model.image;
import com.dineshonjava.model.recordcomment;
import com.dineshonjava.model.scancopy;
import com.dineshonjava.model.selflifestudy;
import com.dineshonjava.model.Records;
import com.dineshonjava.model.chief_login;
import com.dineshonjava.model.coldtest;
import com.dineshonjava.model.coldtestdays;
import com.dineshonjava.model.analysis;
import com.dineshonjava.model.analyticalstudy;
import com.dineshonjava.model.ats;
import com.dineshonjava.model.atsstudies;

/**
 * @author Dinesh Rajput

 */
public interface EmployeeService {
	


	public  List<chief_login> listChief();
	
	
	           /*varsha*/
	
	    public void addAnalysis(analysis analysis);
		
		public List<analysis> listAnalysis();

		public analysis getAnalysis(int id);

		public void deleteAnalysis(analysis analysisBean);
		
		public List<analysis> getAnalysisList(int result, int offset_real);
		
		public int getAnalysis_Size();
		
		
		
		
		public void addFormulation(formulation formulation);

		public List<formulation> listFormulation();	

		public formulation getFormulation(int id);	

		public void deleteFormulation(formulation formulationBean);
		
		public List<formulation> getFormulationList(int result, int offset_real);
		
		public int getFormulationSize();
		
		
		
		public void addATS(ats atsBean);
		
		public List<ats> listATS();

		public ats getATS(int id);

		public void deleteATS(ats atsBean);
		
		public List<ats> getATSList(int result, int offset_real);
		
		public int getATS_Size();
		
		
		public void addColdTestDays(coldtestdays coldtestdaysBean);
		
		public List<coldtestdays> listColdTestDays();

		public coldtestdays getColdTestDays(int id);

		public void deleteColdTestDays(coldtestdays coldtestdaysBean);
		
		public List<coldtestdays> getColdTestDaysList(int result, int offset_real);
		
		public int getColdTestDays_Size();
		
		
		

		public void addImage(image imageBean);

		public List<image> listImage();
		
		public List<image> listImage(int rid);

		public image getImage(int id);

		public void deleteImage(image imageBean);
		
	
		 /*varsha*/
	
	
	
		 /*bhushan*/

	public void addRecords(Records records);

	public List<Records> listRecords();
	
	public Records getRecords(int rid);
	
	public void deleteRecord(Records records);
	
	public List<Records> getRecordsList(int result, int offset_real);
	
	public int getRecords_Size();
	

	
	

	public List<Active> listActive();
	
	public List<Active> listActive(int rid);

	public void addActive(Active active);

	public void deleteActive(Active active);

	public Active getActive(int active_id);
	

	
	
	

	public List<Composition> listComposition();
	
	public List<Composition> listComposition(int rid);

	public void addComposition(Composition composition);

	public void deleteComposition(Composition composition);

	public Composition getComposition(int conposition_id);
	
		
	
	public void addSelfLife_Studies(selflifestudy selflifestudy);
	
	public List<selflifestudy> listSelfLife_Studies();

	public List<selflifestudy> listSelfLife_Studies(int rid);

	public void deleteSelfLife_Studies(selflifestudy selflifestudy);

	public selflifestudy getSelfLife_Studies(int selflife_id);
	
	
	
	public void addAnalytical_Studies(analyticalstudy analyticalstudy);
	
	public void addAnalytical_Studies1(int analyticalstudy_id, String active, String concentration , int rid);
	
	public void updateAnalytical_Studies1(int analyticalstudy_id, String active, String concentration, int rid);
	
	public void deleteAnalytical_Studies1(String active, int rid);
	
	public void deleteAnalytical_Studies1(int analyticalstudy_id);

	public List<analyticalstudy> listAnalytical_Studies();
	
	public List<analyticalstudy> listAnalytical_Studies(int rid);

	public void deleteAnalytical_Studies(analyticalstudy analyticalstudy);

	public analyticalstudy getAnalytical_Studies(int analyticalstudy_id);
	
	
	
	

	public List<scancopy> listScanned_Copies();
	
	public List<scancopy> listScanned_Copies(int rid);

	public void addScanned_Copies(scancopy scancopy);

	public void deleteScanned_Copies(scancopy scancopy);

	public scancopy getScanned_Copies(int scan_id);
	
	
	

	public List<coldtest> listCold_Test();
	
	public List<coldtest> listCold_Test(int rid);

	public void addCold_Test(coldtest coldtest);

	public void deleteCold_Test(coldtest coldtest);

	public coldtest getCold_Test(int cold_id);	
	

	
	public List<atsstudies> listATS_Studies();
	
	public List<atsstudies> listATS_Studies(int rid);
	
	public atsstudies getATS_Studies(int ats_id);

	public void deleteATS_Studies(atsstudies atsstudies);

	public void addATS_Studies(atsstudies atsstudies);
	
	// Search  Of Chief Records Only
	
	public List<Records> ChiefRecordSerchBy_Status_Active(String status, String active, String username);


	public List<Records> ChiefRecordSerchBy_Fomulation_Active(String formulation, String active, String username);


	public List<Records> ChiefRecordSerchBy_Fomulation_Status(String formulation, String status, String username);


	public List<Records> ChiefRecordSerchBy_Fomulation_Status_Active(String formulation, String status, String active, String username);
	
	
	public List<Records> ChiefRecordSerchByFormulation(String formulation, String username);
	
	public List<Records> ChiefRecordSerchByActive(String active, String username);
	
	public List<Records> ChiefRecordSerchByStatus(String status, String username);
	
	public List<Records> ChiefRecordSerchByDate(Date todate, Date fromdate, String username);// fetch record between two dates
	
	
	public List<Records> ChiefRecordSerchBySingleDate(Date todate, Date fromdate, String username);// fetch record by single date
	
	public List<Records> listChiefRecord(String username);
	// Search  Of Chief Records Only
	// for All Executive records
	
	public List<Records> SerchByDate(LocalDate parsedDate, LocalDate parsedDate2);
	
	
	public List<Records> SerchBy_Status_Active(String status, String active);


	public List<Records> SerchBy_Fomulation_Active(String formulation, String active);


	public List<Records> SerchBy_Fomulation_Status(String formulation, String status);


	public List<Records> SerchBy_Fomulation_Status_Active(String formulation, String status, String active);
	
	
	public List<Records> SerchByFormulation(String formulation);
	
	public List<Records> SerchByActive(String active);
	
	public List<Records> SerchByStatus(String status);
	
	/*public List<Records> SerchByDate(Date todate, Date fromdate ,String username);// fetch record between two dates
*/	
	
	public List<Records> SerchBySingleDate(LocalDate parsedDate, LocalDate parsedDate2);// fetch record by single date
	
	// for All Executive records
	
	
	public List<Records> TodayRecordCount(Date format) throws HibernateException, ParseException;// for referance number
	
    // for Preview
	
	public List<Active> List_Chief_Preview_Active(int rid);


	public List<Composition> List_Chief_Preview_Composition(int rid);
	
	
    public List<analyticalstudy> List_Chief_Preview_Analytical_Studies(int rid);
	
	public List<atsstudies> List_Chief_Preview_ATS_Studies(int rid);
	
	public List<coldtest> List_Chief_Preview_Cold_Test(int rid);
	
	
	 //  Preview
	
	//Record Comment Start
	
	public void addRecord_Comment(recordcomment recordcomment);


	public List<recordcomment> getRecord_Comment(int rid);


	public void DeleteRecord_Comment(recordcomment recordcomment);
	
	// record Comments End
	public List<chief_login> ListChiefLogin();
	
	public List<Records> ListRecords();
  
	
	
	public void addATS_Studies1(int ats_id, String active, String concentration, int rid);

	public void deleteATS_Studies1(String activevalue, int rid);

	public void deleteATS_Studies1(int ats_id);

	public void updateATS_Studies1(int ats_id, String active, String concentration, int rid);

	     /*bhushan*/
	

	/* Dipali */
	
	public void addManager(chief_login manager);
	
	public void addchief(chief_login bean);

	public List<chief_login> getEmployeeList(String Designation);
	public chief_login getEmp(int emp_id);
	public void deleteEmp(chief_login managerBean);
	public List<Records> SerchByDate1(LocalDate parsedDate,LocalDate parsedDate2);
	public List<Records> listRemainder1(String username);
	/*Dipali*/
	public List<Records> listRemainder();




	
	


	


	


	


	


	

    




	





	


	


	
	
}
