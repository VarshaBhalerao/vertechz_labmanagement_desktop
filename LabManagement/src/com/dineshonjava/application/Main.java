package com.dineshonjava.application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
	try {

			//Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/Analysis.fxml"));

			Parent root = FXMLLoader.load(getClass().getResource("/com/dineshonjava/fxml_Resources/SearchByDate.fxml"));

			Scene scene = new Scene(root);
			//scene.getStylesheets().add(getClass().getResource("/com/dineshonjava/fxml_Resources/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		
		} catch(Exception e) {
			e.printStackTrace();
		}
	}	
	public static void main(String[] args) {
		launch(args);
	}
}
